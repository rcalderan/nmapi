const MongoDb = require('./database/strategies/mongodb/mongodb')
const Context = require('./database/strategies/base/contextStrategy')

const ClienteSchema = require('./database/strategies/mongodb/mongoDb/schemas/clienteSchemma')
const ClienteRoutes = require('./routes/clienteRoutes')

const RoupaSchema = require('./database/strategies/mongodb/mongoDb/schemas/roupaSchema')
const RoupaRoutes = require('./routes/roupaRoutes')

const ImpressoSchema = require('./database/strategies/mongodb/mongodb/schemas/impressoSchema')
const ImpressoRoutes = require('./routes/impressoRoutes')

const AcessorioSchema = require('./database/strategies/mongodb/mongodb/schemas/contratoItemAcessorioSchema')
const AcessorioRoutes = require('./routes/acessorioRoutes')

const ConfigSchema = require('./database/strategies/mongodb/mongodb/schemas/confSchema')
const ConfigRoutes = require('./routes/confRoutes')

const TipoRoupaSchema = require('./database/strategies/mongodb/mongodb/schemas/roupaTipoSchema')
const TipoRoupaRoutes = require('./routes/tipoRoupaRoutes')

const PrecoSchema = require('./database/strategies/mongodb/mongodb/schemas/precoSchema')
const PrecoRoutes = require('./routes/precoRoutes')

const LogSchema = require('./database/strategies/mongodb/mongodb/schemas/logSchema')
const LogRoutes = require('./routes/logRoutes')

const ContratoSchema = require('./database/strategies/mongodb/mongodb/schemas/contratoSchema')
const ContratoRoutes = require('./routes/contratoRoutes')




const Hapi = require('hapi')

const app = new Hapi.Server({
    port: 5000,
    host: '127.0.0.1'
})

function mapRoutes(instance, methods){
    return methods.map(method => instance[method]())

}

async function main(){
    const connection = MongoDb.connect()
    const clientContext = new Context(new MongoDb(connection,ClienteSchema))
    const roupaContext = new Context(new MongoDb(connection,RoupaSchema))
    const impressoContext = new Context(new MongoDb(connection,ImpressoSchema))
    const acessorioContext = new Context(new MongoDb(connection,AcessorioSchema))
    const confContext = new Context(new MongoDb(connection,ConfigSchema))
    const tipoRoupaContext = new Context(new MongoDb(connection,TipoRoupaSchema))
    const precoContext = new Context(new MongoDb(connection,PrecoSchema))
    const logContext = new Context(new MongoDb(connection,LogSchema))
    const contratoContext = new Context(new MongoDb(connection,ContratoSchema))

    app.route([
        ...mapRoutes(new ClienteRoutes(clientContext),ClienteRoutes.methods()),
        ...mapRoutes(new RoupaRoutes(roupaContext),RoupaRoutes.methods()),
        ...mapRoutes(new ImpressoRoutes(impressoContext),ImpressoRoutes.methods()),
        ...mapRoutes(new AcessorioRoutes(acessorioContext),AcessorioRoutes.methods()),
        ...mapRoutes(new ConfigRoutes(confContext),ConfigRoutes.methods()),
        ...mapRoutes(new TipoRoupaRoutes(tipoRoupaContext),TipoRoupaRoutes.methods()),
        ...mapRoutes(new PrecoRoutes(precoContext),PrecoRoutes.methods()),
        ...mapRoutes(new LogRoutes(logContext),LogRoutes.methods()),
        ...mapRoutes(new ContratoRoutes(contratoContext),ContratoRoutes.methods()),
    ])

    await app.start()
    console.log('Api rodando na porta',app.info.port)
    return app
}


module.exports = main()