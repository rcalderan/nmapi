const assert = require('assert').strict
const MongoDb = require('../database/strategies/mongodb/mongoDb')
const BandeiraSchema = require('../database/strategies/mongodb/mongodb/schemas/bandeiraSchema')
const Context = require('../database/strategies/base/contextStrategy')

var MOCK_CADASTRAR = {
    _id: 0,
    nome: "Miiiirrr",
    sigla: "Mir",
    isActive: true,
};


let context = {}
let lastId = 0
describe('Teste para os tipos de pagamento "Bandeira"', function () {
    this.beforeAll(async () => {
        const connection = MongoDb.connect()
        context = new Context(new MongoDb(connection, BandeiraSchema))
        var gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0
    })
    

    it('Cadastrar Forma', async function () {
        const resultado = await context.create(MOCK_CADASTRAR)
        var gotId = await context.lastId()
        
        lastId = gotId ? gotId._id++ : 0

        assert.ok(resultado._id=== lastId)
    })

    it('Listar Forma: ', async function () {
        const [result] = await context.read({ _id: lastId })

        assert.ok(result._id===lastId)
    })

    it('Listar Todas Formas: ', async function () {
        const result = await context.read()
        
        assert.ok(Array.isArray(result))
    })

    it('Atualizar Forma: ', async () => {
        const result = await context.update(lastId, {
            nome: MOCK_CADASTRAR.nome+ Math.random().toString()
        })
        assert.deepEqual(result.nModified, 1)
    })
    
    it('Remover Forma: ', async () => {
        const result = await context.delete(lastId)
        assert.deepEqual(result.n, 1)
    })
})
