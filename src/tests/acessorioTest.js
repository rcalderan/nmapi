const assert = require('assert').strict
const MongoDb = require('../database/strategies/mongodb/mongoDb')
const AcessorioSchema = require('../database/strategies/mongodb/mongodb/schemas/contratoItemAcessorioSchema')
const Context = require('../database/strategies/base/contextStrategy')

const Api = require('../api')

var MOCK_A_CADASTRAR = { 
    _id: 1,
    sigla :'C42',
    nome: 'CAMISA 42',
};

let context = {}

let lastId = 0
describe('Testes de Acessorios', function () {
    this.beforeAll(async () => {
        const connection = MongoDb.connect()
        context = new Context(new MongoDb(connection, AcessorioSchema))
        const gotId = await context.lastId()
        lastId = gotId ? gotId._id :0

    })
    it('Cadastrar Acessorio', async function() {
        const resultado = await context.create(MOCK_A_CADASTRAR)
        const gotId = await context.lastId()
        lastId = gotId ? gotId._id :0

        assert.ok( resultado._id===lastId)
    })

    it('Listar Acessorio', async function () {
        const [result] = await context.read({ _id: lastId},0,10)
        
        assert.ok(result._id===lastId)
    })
    
    it('Atualizar Acessorio', async () => {
        var rand = Math.random()
        const result = await context.update(lastId, {
            nome: MOCK_A_CADASTRAR.nome+rand.toString()
        })
        assert.deepEqual(result.nModified, 1)
    })
    it('Remover Acessorio', async () => {
        const result = await context.delete(lastId)
        assert.deepEqual(result.n, 1)
    })
})

let app ={}
let atID ={}
describe('acessorio Routes', function (){
    this.beforeAll(async ()=>{
        app = await Api

    })

    it('/acessorio -> get - listar',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/acessorio?skip=0&limit=10'
        })
        const dados = JSON.parse(result.payload)
        
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })
    
    
    it('/acessorio -> ListAll',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/acessorio/all'
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })

    it('/acessorio -> get - listar por sigla',async ()=>{
        const sigla='C40'
        const result = await app.inject({
            method:'GET',
            url: `/acessorio/${sigla}`
        })
        const [dados]= JSON.parse( result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.deepEqual(dados.sigla, sigla)
    })

    it('Cadastrar acessorio- POST', async ()=>{
        MOCK_A_CADASTRAR.nome=MOCK_A_CADASTRAR.nome+' (Via POST)'
        const result = await app.inject({
            method:'POST',
            url:'/acessorio',
            payload: MOCK_A_CADASTRAR
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id
        assert.ok(statusCode===200)
        assert.notStrictEqual(_id,undefined)
        assert.deepEqual(message,"Acessorio cadastrado com sucesso!")
    })

    
    it('Atualizar acessorio- PATCH', async ()=>{
        const id = atID
        const expected = {
            nome: MOCK_A_CADASTRAR.nome+' (Atualizado)'
        } 
        const result = await app.inject({
            method:'PATCH',
            url:`/acessorio/${id}`,
            payload: JSON.stringify(expected)
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Acessorio atualizado com sucesso!")
    })


    
    it('Remover acessorio- DELETE', async ()=>{
        const id = atID
        const result = await app.inject({
            method:'DELETE',
            url:`/acessorio/${id}`,
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id;
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Acessorio removido com sucesso!")
    })
})
