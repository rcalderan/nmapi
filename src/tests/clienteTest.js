const assert = require('assert').strict
const MongoDb = require('../database/strategies/mongodb/mongoDb')
const ClienteSchema = require('../database/strategies/mongodb/mongoDb/schemas/clienteSchemma')
const Context = require('../database/strategies/base/contextStrategy')

const Api = require('../api')


var MOCK_A_CADASTRAR = { 
    _id: 1,
    nome: 'Richard Carvalho Calderan Kaibara',
    cpf:'326.972.218-40',
    rg: '32625168-6',
    nascimento : new Date('1981-03-11 00:00:00'),
    sexo: true,
    autenticacao:false,
    fones:[16997292729,1633722663],
    obs:'nada a declarar',
    email:'richardcck@hotmail.com',
    por: 1,
    endereco: {
        cep:'13560-642',
        logradouro: 'rua 13 de maio',
        numero: 1558,
        bairro: 'Jardim São Carlos',
        cidade:'São Carlos',
        uf: 'SP'
    }
};

var MOCK_CLIENTE_ATUALIZAR = { 
    _id: 1,
    nome: 'Richard Carvalho Calderan',
    cpf:'326.972.218-40',
    rg: '32625168-6',
    nascimento : new Date('1981-03-11 00:00:00'),
    sexo: true,
    autenticacao:false,
    fones:[16997292729,1633722663],
    obs:'nada a declarar',
    email:'richardcck@hotmail.com',
    por: 1,
    endereco: {
        cep:'13560-642',
        logradouro: 'rua 13 de maio',
        numero: 1558,
        bairro: 'Jardim São Carlos',
        cidade:'São Carlos',
        uf: 'SP'
    }
};
let context = {}

describe('MongoDB Suite de testes', function () {
    this.beforeAll(async () => {
        const connection = MongoDb.connect()
        context = new Context(new MongoDb(connection, ClienteSchema))
    })
    it('verificar conexao', async () => {
        const result = await context.isConnected()
        const expected = 'Conectado'

        assert.deepEqual(result, expected)
    })
})

let lastId = 0
describe('Testes de cliente', function () {
    this.beforeAll(async () => {
        const gotId = await context.lastId()
        lastId = gotId ? gotId._id :0

    })
    it('Cadastrar Cliente', async function() {
        const resultado = await context.create(MOCK_A_CADASTRAR)
        const gotId = await context.lastId()
        lastId = gotId ? gotId._id :0

        assert.ok( resultado._id === lastId)
    })

    it('Listar Cliente', async function () {
        const [result] = await context.read({ _id: MOCK_A_CADASTRAR._id},0,10)
        
        //console.log(JSON.stringify(result))
        //assert.deepEqual(result, MOCK_CLIENTE_CADASTRAR)
        assert.ok( result._id==lastId)
    })
    it('Count: ', async function () {
        const result = await context.count()
        assert.notDeepStrictEqual(isNaN(result))
    })
    
    it('Atualizar Cliente', async () => {
        var rand = Math.random()
        const result = await context.update(lastId, {
            nome: MOCK_CLIENTE_ATUALIZAR.nome+rand.toString()
        })
        assert.deepEqual(result.nModified, 1)
    })
    it('Remover Cliente', async () => {
        if(!lastId)
        console.log('Não pode exluir id=0')
        
        const result = await context.delete(lastId)
        assert.deepEqual(result.n, 1)
    })

})

let app ={}
let atID ={}
describe('Cliente Routes', function (){
    this.beforeAll(async ()=>{
        app = await Api

    })

    it('/cliente -> get - listar',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/cliente?skip=0&limit=10'
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })
    
    
    it('/cliente -> ListAllNames',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/cliente/names'
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })

    it('/cliente -> get - listar por id',async ()=>{
        const id=1
        const result = await app.inject({
            method:'GET',
            url: `/cliente/${id}`
        })
        const [dados]= JSON.parse( result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.deepEqual(dados._id, id)
    })
    it('/cliente -> get - retrnar not found caso nao encontre',async ()=>{
        const id=200000
        const result = await app.inject({
            method:'GET',
            url: `/cliente/${id}`
        })
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,404)
    })
    it('Lista Clientes, deve falhar ao passar type errado de Limit', async ()=>{
        const _LIMIT_ERRADO='aaaaa'
        const result = await app.inject({
            method:'GET',
            url: `/cliente?skip=0&limit=${_LIMIT_ERRADO}`
        })
        
        const dados = JSON.parse(result.payload)
        assert.deepEqual(dados.statusCode,400)
    })
    it('Lista Clientes, filtra por nome', async ()=>{
        const NOME='ich'
        const result = await app.inject({
            method:'GET',
            url: `/cliente?skip=0&limit=10&nome=${NOME}`
        })
        
        const dados = JSON.parse(result.payload)
        
        assert.ok(Array.isArray(dados))
    })

    it('Lista Clientes, filtra por CPF', async ()=>{
        const CPF='326.972.218-41'
        const result = await app.inject({
            method:'GET',
            url: `/cliente?skip=0&limit=10&cpf=${CPF}`
        })
        
        const statusCode = result.statusCode
        const dados = JSON.parse(result.payload)
        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })
    it('Lista Clientes, deve retornar 3 resgistros', async ()=>{
        const _TAMANHO_LIMITE=3
        const result = await app.inject({
            method:'GET',
            url: `/cliente?skip=0&limit=${_TAMANHO_LIMITE}`
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode
        assert.deepEqual(statusCode,200)
        assert.ok(dados.length ===_TAMANHO_LIMITE)
    })

    it('Cadastrar Cliente- POST Clientes', async ()=>{
        MOCK_A_CADASTRAR.nome=MOCK_A_CADASTRAR.nome+' (Via POST)'
        const result = await app.inject({
            method:'POST',
            url:'/cliente',
            payload: MOCK_A_CADASTRAR
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id
        assert.ok(statusCode===200)
        assert.notStrictEqual(_id,undefined)
        assert.deepEqual(message,"Cliente cadastrado com sucesso!")
    })

    
    it('Atualizar Cliente- PATCH Clientes', async ()=>{
        const id = atID
        const expected = {
            nome: MOCK_A_CADASTRAR.nome+' (Atualizado)'
        } 
        const result = await app.inject({
            method:'PATCH',
            url:`/cliente/${id}`,
            payload: JSON.stringify(expected)
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Cliente atualizado com sucesso!")
    })

    
    it('Atualizar Cliente- PATCH Clientes- nao deve atualizar id inesistente', async ()=>{
        const id = 10000
        const expected = {
            nome: MOCK_A_CADASTRAR.nome+' (Atualizado)'
        } 
        const result = await app.inject({
            method:'PATCH',
            url:`/cliente/${id}`,
            payload: JSON.stringify(expected)
        })
        const statusCode = result.statusCode
        const {message}= JSON.parse( result.payload)
        assert.ok(statusCode===412)
        assert.deepEqual(message,'Não foi possível atualizar cliente!')
    })

    
    it('Remover Cliente- DELETE Clientes', async ()=>{
        const id = atID
        const result = await app.inject({
            method:'DELETE',
            url:`/cliente/${id}`,
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id;
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Cliente removido com sucesso!")
    })
})
