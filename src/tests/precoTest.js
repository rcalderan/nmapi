const assert = require('assert').strict
const MongoDb = require('../database/strategies/mongodb/mongoDb')
const TipoRoupaSchema = require('../database/strategies/mongodb/mongodb/schemas/precoSchema')
const Context = require('../database/strategies/base/contextStrategy')
const Api = require('../api')

var MOCK_CADASTRAR = {
    _id:100,
    nome:"Gold",
    cor:"gold",
    tipo:3,
    valor:690,
    enabled:true
}


let context = {}
let lastId = 0
describe('Testes preco', function () {
    this.beforeAll(async () => {
        const connection = MongoDb.connect()
        context = new Context(new MongoDb(connection, TipoRoupaSchema))
        var gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0
    })
    it('Cadastrar preço', async function () {
        const resultado = await context.create(MOCK_CADASTRAR)
        var gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0

        assert.ok(resultado._id=== lastId)
    })

    it('Listar preço: ', async function () {
        const [result] = await context.read({ _id: lastId })

        assert.ok(result._id===lastId)
    })

    it('Listar Todos preços: ', async function () {
        const result = await context.read()

        assert.ok(Array.isArray(result))
    })

    it('Atualizar preço: ', async () => {
        const result = await context.update(lastId, {
            nome: Math.random().toString()
        })
        assert.deepEqual(result.nModified, 1)
    })
    
    it('Remover preço: ', async () => {
        var lastId = await context.lastId()
        const result = await context.delete(lastId)
        assert.deepEqual(result.n, 1)
    })
})

let app ={}
let atID ={}
describe('Preco Routes', function (){
    this.beforeAll(async ()=>{
        app = await Api

    })

    it('/preco -> get - listar',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/preco?&skip=0&limit=10'
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode

        console.log('length ',dados.length);
        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })
    

    it('/preco -> get - listar por tipo',async ()=>{
        const tipo=1
        const result = await app.inject({
            method:'GET',
            url: `/preco/${tipo}`
        })
        const dados= JSON.parse( result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(dados.length >= 1)
    })

    it('Cadastrar preco- POST', async ()=>{
        const result = await app.inject({
            method:'POST',
            url:'/preco',
            payload: MOCK_CADASTRAR
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        
        atID = _id
        assert.ok(statusCode===200)
        assert.notStrictEqual(_id,undefined)
        assert.deepEqual(message,"Preco cadastrado com sucesso!")
    })

    
    it('Atualizar preco- PATCH', async ()=>{
        const id = atID
        const expected = {
            nome: 'atualizado'
        } 
        const result = await app.inject({
            method:'PATCH',
            url:`/preco/${id}`,
            payload: JSON.stringify(expected)
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Preco atualizado com sucesso!")
    })

    
    it('Remover preco- DELETE', async ()=>{
        const id = atID
        const result = await app.inject({
            method:'DELETE',
            url:`/preco/${id}`,
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id;
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Preco removido com sucesso!")
    })
})
