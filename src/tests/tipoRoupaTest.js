const assert = require('assert').strict
const MongoDb = require('../database/strategies/mongodb/mongoDb')
const TipoRoupaSchema = require('../database/strategies/mongodb/mongodb/schemas/roupaTipoSchema')
const Context = require('../database/strategies/base/contextStrategy')
const Api = require('../api')

var MOCK_CADASTRAR =
    { _id: 6, codigo: "MR", nome: "Mirr", descricao: "Mirr du Piirrr" }


let context = {}
let lastId = 0
describe('Testes Tipo Roupa', function () {
    this.beforeAll(async () => {
        const connection = MongoDb.connect()
        context = new Context(new MongoDb(connection, TipoRoupaSchema))
        var gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0
    })
    it('Cadastrar Tipo Roupa', async function () {
        const resultado = await context.create(MOCK_CADASTRAR)
        var gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0

        assert.ok(resultado._id=== lastId)
    })

    it('Listar Tipo Roupa: ', async function () {
        const [result] = await context.read({ _id: lastId })

        assert.ok(result._id===lastId)
    })

    it('Listar Todos Tipos Roupa: ', async function () {
        const result = await context.read()

        assert.ok(Array.isArray(result))
    })

    it('Atualizar Tipo Roupa: ', async () => {
        const result = await context.update(lastId, {
            nome: Math.random().toString()
        })
        assert.deepEqual(result.nModified, 1)
    })
    
    it('Remover Tipo Roupa: ', async () => {
        var lastId = await context.lastId()
        const result = await context.delete(lastId)
        assert.deepEqual(result.n, 1)
    })
})


let app ={}
let atID ={}
describe('TipoRoupa Routes', function (){
    this.beforeAll(async ()=>{
        app = await Api

    })

    it('/tiporoupa -> get - listar',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/tiporoupa?skip=0&limit=10'
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })
    
    
    it('/tiporoupa -> ListAllNames',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/tiporoupa/names'
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })

    it('/tiporoupa -> get - listar por id',async ()=>{
        const id=1
        const result = await app.inject({
            method:'GET',
            url: `/tiporoupa/${id}`
        })
        const [dados]= JSON.parse( result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.deepEqual(dados._id, id)
    })
    it('/tiporoupa -> get - retrnar not found caso nao encontre',async ()=>{
        const id=2000
        const result = await app.inject({
            method:'GET',
            url: `/tiporoupa/${id}`
        })
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,404)
    })
    it('Lista tiporoupa, deve falhar ao passar type errado de Limit', async ()=>{
        const _LIMIT_ERRADO='aaaaa'
        const result = await app.inject({
            method:'GET',
            url: `/tiporoupa?skip=0&limit=${_LIMIT_ERRADO}`
        })
        
        const dados = JSON.parse(result.payload)
        assert.deepEqual(dados.statusCode,400)
    })
    it('Lista tiporoupa, filtra por nome', async ()=>{
        const NOME='ich'
        const result = await app.inject({
            method:'GET',
            url: `/tiporoupa?skip=0&limit=10&nome=${NOME}`
        })
        
        const dados = JSON.parse(result.payload)
        
        assert.ok(Array.isArray(dados))
    })

    it('Cadastrar tiporoupa- POST', async ()=>{
        MOCK_CADASTRAR.nome=MOCK_CADASTRAR.nome+' (Via POST)'
        const result = await app.inject({
            method:'POST',
            url:'/tiporoupa',
            payload: MOCK_CADASTRAR
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id
        assert.ok(statusCode===200)
        assert.notStrictEqual(_id,undefined)
        assert.deepEqual(message,"TipoRoupa cadastrado com sucesso!")
    })

    
    it('Atualizar tiporoupa- PATCH', async ()=>{
        const id = atID
        const expected = {
            descricao: MOCK_CADASTRAR.descricao+' (Atualizado)'
        } 
        const result = await app.inject({
            method:'PATCH',
            url:`/tiporoupa/${id}`,
            payload: JSON.stringify(expected)
        })
        const statusCode = result.statusCode
        const {message}= JSON.parse( result.payload)
        assert.ok(statusCode===200)
        assert.deepEqual(message,"TipoRoupa atualizado com sucesso!")
    })

    
    it('Remover tiporoupa- DELETE', async ()=>{
        const id = atID
        const result = await app.inject({
            method:'DELETE',
            url:`/tiporoupa/${id}`,
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id;
        assert.ok(statusCode===200)
        assert.deepEqual(message,"TipoRoupa removido com sucesso!")
    })
})
