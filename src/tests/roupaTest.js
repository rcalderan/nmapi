const assert = require('assert').strict
const MongoDb = require('../database/strategies/mongodb/mongoDb')
const RoupaSchema = require('../database/strategies/mongodb/mongoDb/schemas/roupaSchema')
const Context = require('../database/strategies/base/contextStrategy')
const Api = require('../api')

var MOCK_CADASTRAR = {
    _id: 1,
    nome: "VESTIDO AZUL ROYAL 48",
    valor: 390,
    tamanho: "48",
    cor: "blue",
    base: 390,
    data: "2019-01-09 12:26:41.571",
    preco_id: 0,
    status: 1,
    tipo: 0,
};

let context = {}
let lastId = 0
describe('Testando collection ROUPA', function () {
    this.beforeAll(async () => {
        const connection = MongoDb.connect()
        context = new Context(new MongoDb(connection, RoupaSchema))
        var gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0
    })
    it('Cadastrar Roupa', async function () {
        const resultado = await context.create(MOCK_CADASTRAR)
        
        var gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0
        assert.ok(resultado._id=== lastId)
    })

    it('Listar Roupa: ', async function () {
        const [result] = await context.read({ _id: lastId })

        assert.ok(result._id===lastId)
    })

    it('Listar Todas: ', async function () {
        const result = await context.read()

        assert.ok(Array.isArray(result))
    })

    it('Atualizar Roupa: ', async () => {
        const result = await context.update(lastId, {
            nome: Math.random().toString()
        })
        assert.deepEqual(result.nModified, 1)
    })
    
    it('Remover roupa', async () => {

        const result = await context.delete(lastId)
        assert.deepEqual(result.n, 1)
    })
})

let app ={}
let atID ={}
describe('Roupa Routes', function (){
    this.beforeAll(async ()=>{
        app = await Api

    })

    it('/roupa -> get - listar',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/roupa?skip=0&limit=10'
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })

    it('/roupa -> get - listar por id',async ()=>{
        const id=1
        const result = await app.inject({
            method:'GET',
            url: `/roupa/${id}`
        })
        const [dados]= JSON.parse( result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.deepEqual(dados._id, id)
    })
    it('/roupa -> get - retornar not found caso nao encontre',async ()=>{
        const id=2000000
        const result = await app.inject({
            method:'GET',
            url: `/roupa/${id}`
        })
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,404)
    })
    it('Lista roupas, deve falhar ao passar type errado de Limit', async ()=>{
        const _LIMIT_ERRADO='aaaaa'
        const result = await app.inject({
            method:'GET',
            url: `/roupa?skip=0&limit=${_LIMIT_ERRADO}`
        })
        
        const dados = JSON.parse(result.payload)
        assert.deepEqual(dados.statusCode,400)
    })
    it('Lista roupa, filtra por nome', async ()=>{
        const NOME='ich'
        const result = await app.inject({
            method:'GET',
            url: `/roupa?skip=0&limit=10&nome=${NOME}`
        })
        
        const dados = JSON.parse(result.payload)
        
        assert.ok(Array.isArray(dados))
    })


    it('Cadastrar roupa- POST ', async ()=>{
        MOCK_CADASTRAR.nome=MOCK_CADASTRAR.nome+' (Via POST)'
        const result = await app.inject({
            method:'POST',
            url:'/roupa',
            payload: MOCK_CADASTRAR
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id
        assert.ok(statusCode===200)
        assert.notStrictEqual(_id,undefined)
        assert.deepEqual(message,"Roupa cadastrada com sucesso!")
    })

    
    it('Atualizar roupa- PATCH', async ()=>{
        const id = atID
        const expected = {
            nome: MOCK_CADASTRAR.nome+' (Atualizado)'
        } 
        const result = await app.inject({
            method:'PATCH',
            url:`/roupa/${id}`,
            payload: JSON.stringify(expected)
        })
        
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Roupa atualizada com sucesso!")
    })

    
    it('Atualizar roupa- PATCH roupa- nao deve atualizar id inesistente', async ()=>{
        const id = 10000
        const expected = {
            nome: MOCK_CADASTRAR.nome+' (Atualizado)'
        } 
        const result = await app.inject({
            method:'PATCH',
            url:`/roupa/${id}`,
            payload: JSON.stringify(expected)
        })
        const statusCode = result.statusCode
        const {message}= JSON.parse( result.payload)
        assert.ok(statusCode===412)
        assert.deepEqual(message,'Não foi possível atualizar roupa!')
    })

    
    it('Remover roupa- DELETE roupa', async ()=>{
        const id = atID
        const result = await app.inject({
            method:'DELETE',
            url:`/roupa/${id}`,
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id;
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Roupa removido com sucesso!")
    })
})
