const assert = require('assert').strict
const MongoDb = require('../database/strategies/mongodb/mongoDb')
const ConfSchema = require('../database/strategies/mongodb/mongodb/schemas/confSchema')
const Context = require('../database/strategies/base/contextStrategy')
const Api = require('../api')



const MOCK_CONF ={_id:1,
    criar_recibos:true,
    backupPath:'f:\"',
    cnpj:"08.299.621/0001-20",
    estadual:"637.287.665.118",
    municipal:"51.197",
    empresa:"C & K LOCACAO DE ROUPAS LTDA-ME",
    fantasia:"Noiva Modas",
    endereco:"Rua Jesuíno de Arruda",
    numero:1837,bairro:"Centro",cidade:"São Carlos",
    uf:"SP",cep:"13560-642",telefone1:"(16) 33722363",
    telefone2:"(16) 99702-7631",
    email:"noivamodas@live.com",
    site:"www.noivamodas.com.br",
    }

let context = {}
let lastId = 0
describe('Teste de configurações', function () {
    this.beforeAll(async () => {
        const connection = MongoDb.connect()
        context = new Context(new MongoDb(connection, ConfSchema))
        var gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0
    })
    it('Cadastrar conf', async function () {
        
        const resultado = await context.create(MOCK_CONF)
        
        lastId ++

        assert.ok(resultado._id===lastId)
    })

    it('Listar conf: ', async function () {
        const [result] = await context.read({ _id: lastId })
        assert.ok(result._id===lastId)
    })
    
    it('Remover conf: ', async () => {
        if(lastId===1)
            assert.ok(true)
        const result = await context.delete(lastId)
        assert.deepEqual(result.n, 1)
    })

})

let app ={}
let atID ={}
describe('Config Routes', function (){
    this.beforeAll(async ()=>{
        app = await Api

    })
    

    it('/acessorio -> get - listar o marcado como default',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: `/conf`
        })
        const [dados]= JSON.parse( result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(dados.isDefault)
    })

    it('Cadastrar configurações - POST', async ()=>{
        MOCK_CONF.fantasia=MOCK_CONF.fantasia+' (Via POST)'
        const result = await app.inject({
            method:'POST',
            url:'/conf',
            payload: MOCK_CONF
        })
        
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id
        assert.ok(statusCode===200)
        assert.notStrictEqual(_id,undefined)
        assert.deepEqual(message,"Configurações cadastradas com sucesso!")
    })

    
    it('Atualizar conf- PATCH', async ()=>{
        const id = atID
        const expected = {
            fantasia: MOCK_CONF.fantasia+' (Atualizado)'
        } 
        const result = await app.inject({
            method:'PATCH',
            url:`/conf/${id}`,
            payload: JSON.stringify(expected)
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Configurações atualizadas com sucesso!")
    })


    
    it('Remover conf- DELETE', async ()=>{
        const id = atID
        const result = await app.inject({
            method:'DELETE',
            url:`/conf/${id}`,
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id;
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Configurações removidas com sucesso!")
    })
})
