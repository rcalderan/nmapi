const assert = require('assert').strict
const MongoDb = require('../database/strategies/mongodb/mongoDb')
const ContratoSchema = require('../database/strategies/mongodb/mongodb/schemas/contratoSchema')
const Context = require('../database/strategies/base/contextStrategy')
const Api = require('../api')


var MOCK_A_CADASTRAR = {
    _id: 1,
    cliente: 1,
    retirada: new Date('1981-03-11 00:00:00'),
    usa: new Date('1981-03-11 00:00:00'),
    devolucao: new Date('1981-03-11 00:00:00'),
    criado_por: 1,
    comunicado: 'Nada a comunicar',
    itens: [{
        codigo: '1',
        descricao: 'Vestido blé',
        valor: 490,
        sub: ['provar com sapato'],
        atendente: 2
    },
    {
        codigo: 'C40',
        descricao: 'CAMISA 40',
        valor: 49.9,
        sub: ['provar!!!!'],
        atendente: 3
    },
    ],
    
    pagamentos: [{
        data: new Date('1981-03-11 00:00:00'),
        valor: 360,
        funcionario: 2,
        recibo: {
            data: new Date('1981-03-11 00:00:00'),
        }
    },{
        data: new Date('1981-03-11 00:00:00'),
        valor: 10,
        funcionario: 2,
        forma:1,
        recibo: {
            data: new Date('1981-03-11 00:00:00'),
        }
    },
    {
        data: new Date('1981-03-11 00:00:00'),
        forma: 4, 
        valor: 60, 
        vezes: 1, 
        funcionario: 1,
    },
    ]
};

let context = {}

let lastId = 0
describe('Testes de Contratos', function () {
    this.beforeAll(async () => {
        const connection = MongoDb.connect()
        context = new Context(new MongoDb(connection, ContratoSchema))
        const gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0

    })
    it('Criar Contrato', async function () {
        const resultado = await context.create(MOCK_A_CADASTRAR)
        const gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0

        assert.ok(resultado._id === lastId)
    })

    it('Atualizar Contrato', async () => {
        var rand = Math.random()
        const result = await context.update(lastId, {
            comunicado: "Algo assim... " + rand.toString()
        })
        assert.deepEqual(result.nModified, 1)
    })
    it('Listar Contrato específico', async function () {
        const [result] = await context.read({ _id: MOCK_A_CADASTRAR._id }, 0, 10)

        assert.ok(result._id == lastId)
    })

    it('Remover Contrato', async () => {

        const result = await context.delete(lastId)
        assert.deepEqual(result.n, 1)
    })
})

let app ={}
let atID ={}
describe('Contrato Routes', function (){
    this.beforeAll(async ()=>{
        app = await Api

    })
    
    it('Cadastrar contrato- POST', async ()=>{
        MOCK_A_CADASTRAR.comunicado=MOCK_A_CADASTRAR.comunicado+' (Via POST)'
        const result = await app.inject({
            method:'POST',
            url:'/contrato',
            payload: MOCK_A_CADASTRAR
        })
        
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id
        assert.ok(statusCode===200)
        assert.notStrictEqual(_id,undefined)
        assert.deepEqual(message,"Contrato cadastrado com sucesso!")
    })

    it('/contrato -> get - get last RECIBO ID',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/contrato/lastRecibo'
        })
        
        const last = JSON.parse(result.payload)
        const statusCode = result.statusCode
        assert.deepEqual(statusCode,200)
        assert.ok(last>0)
    })

    it('/contrato -> get - listar',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/contrato?skip=0&limit=10'
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode
        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })
    
    
    it('/contrato -> ListAllNames',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/contrato/names'
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode
        
        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })

    it('/contrato -> get - listar por id',async ()=>{
        const id=atID;
        const result = await app.inject({
            method:'GET',
            url: `/contrato/${id}?skip=0&limit=10`
        })
        const statusCode = result.statusCode
        const [dados]= JSON.parse( result.payload)

        assert.deepEqual(statusCode,200)
        assert.deepEqual(dados._id, id)
    })
    it('/contrato -> get - retrnar not found caso nao encontre',async ()=>{
        const id=2000000
        const result = await app.inject({
            method:'GET',
            url: `/contrato/${id}`
        })
        const statusCode = result.statusCode
        assert.deepEqual(statusCode,404)
    })

    it('Lista contratos, filtra por data', async ()=>{
        const from= new Date(2019,11,1)//mes 11 == dezembro
        const to= new Date(2019,11,21)
        const result = await app.inject({
            method:'GET',
            url: `/contrato/date?skip=0&limit=10&from=${from}&to=${to}`
        })
        
        const statusCode = result.statusCode
        const dados = JSON.parse(result.payload)
        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })
    it('Lista contratos, datas not found', async ()=>{
        const from= new Date(2019,11,1)//mes 11 == dezembro
        const to= new Date(2019,10,21)
        const result = await app.inject({
            method:'GET',
            url: `/contrato/date?skip=0&limit=10&from=${from}&to=${to}`
        })
        
        const statusCode = result.statusCode
        assert.deepEqual(statusCode,404)
    })
    

    
    it('Atualizar contrato- PATCH', async ()=>{
        const id = atID
        const expected = {
            comunicado: MOCK_A_CADASTRAR.comunicado+' (Atualizado)'
        } 
        const result = await app.inject({
            method:'PATCH',
            url:`/contrato/${id}`,
            payload: JSON.stringify(expected)
        })
        const statusCode = result.statusCode
        const {message}= JSON.parse( result.payload)
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Contrato atualizado com sucesso!")
    })
    
    it('Remover contrato- DELETE', async ()=>{
        const id = atID
        const result = await app.inject({
            method:'DELETE',
            url:`/contrato/${id}`,
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id;
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Contrato removido com sucesso!")
    })
})
