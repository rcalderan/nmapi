const assert = require('assert').strict
const MongoDb = require('../database/strategies/mongodb/mongoDb')
const LogSchema = require('../database/strategies/mongodb/mongodb/schemas/logSchema')
const Context = require('../database/strategies/base/contextStrategy')
const Api = require('../api')

var MOCK_CADASTRAR = {
    _id: 1,
    type:2,
    who:1,
    machine:'DESKTOP',
    what: 'peidou'
};

let context = {}
let lastId = 0
describe('Testando LOG', function () {
    this.beforeAll(async () => {
        const connection = MongoDb.connect()
        context = new Context(new MongoDb(connection, LogSchema))
        var gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0
    })
    it('Cadastrar LOG', async function () {
        const resultado = await context.create(MOCK_CADASTRAR)
        
        var gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0
        assert.ok(resultado._id=== lastId)
    })

    it('Listar LOG: ', async function () {
        const [result] = await context.read({ _id: lastId })

        assert.ok(result._id===lastId)
    })

    it('Listar todos LOGs: ', async function () {
        const result = await context.read()

        assert.ok(Array.isArray(result))
    })

    it('Atualizar LOG: ', async () => {
        const result = await context.update(lastId, {
            what: Math.random().toString()
        })
        assert.deepEqual(result.nModified, 1)
    })
    
    it('Remover LOG: ', async () => {
        var lastId = await context.lastId()
        const result = await context.delete(lastId)
        assert.deepEqual(result.n, 1)
    })
})


let app ={}
let atID ={}
describe('LOG Routes', function (){
    this.beforeAll(async ()=>{
        app = await Api

    })

    it('Emitir log- POST', async ()=>{
        MOCK_CADASTRAR.what=MOCK_CADASTRAR.what+' (Via POST)'
        const result = await app.inject({
            method:'POST',
            url:'/log',
            payload: MOCK_CADASTRAR
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id
        assert.ok(statusCode===200)
        assert.notStrictEqual(_id,undefined)
        assert.deepEqual(message,"LOG emitido com sucesso!")
    })

    it('/log -> get - listar',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/log?skip=0&limit=10'
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(dados.length>0)
    })
    
    
    it('/log -> get - listar por type',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/log?type=2'
        })
        
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(dados.length>0)
    })

    it('/log -> get - listar por MAQUINHA',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/log?machine=DESKTOP'
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(dados.length>0)
    })
    
    it('Lista LOG, filtra por periodo', async ()=>{
        let now= new Date()
        let from= new Date(now.getFullYear(),now.getMonth(),now.getDate(),now.getHours()-1)
        let to= new Date(now.getFullYear(),now.getMonth(),now.getDate(),now.getHours()+1)
        const result = await app.inject({
            method:'GET',
            url: `/log?from=${from}&to=${to}`
        })
        const dados = JSON.parse(result.payload)
        assert.deepEqual(1,dados.length)
    })

    it('Lista log, filtra por QUEM', async ()=>{
        const QUEM=1
        const result = await app.inject({
            method:'GET',
            url: `/log?skip=0&limit=10&who=${QUEM}`
        })
        
        const statusCode = result.statusCode
        const dados = JSON.parse(result.payload)
        assert.deepEqual(statusCode,200)
        assert.ok(dados.length>0)
    })


    it('Remover log- DELETE LOG', async ()=>{
        const id = atID
        const result = await app.inject({
            method:'DELETE',
            url:`/log/${id}`,
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id;
        assert.ok(statusCode===200)
        assert.deepEqual(message,"LOG removido com sucesso!")
    })
})
