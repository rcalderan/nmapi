const assert = require('assert').strict
const MongoDb = require('../database/strategies/mongodb/mongoDb')
const ImpressoSchema = require('../database/strategies/mongodb/mongodb/schemas/impressoSchema')
const Context = require('../database/strategies/base/contextStrategy')
const Api = require('../api')



const cadastrar = {
    _id: 1,
    nome: "Contrato",
    type: "Contrato",
    content: ['Linha1..','linha2'],
    isDefault: true, 
    data: '0000-12-31 22:00:00.000'
}

let context = {}
let lastId = 0
describe('Teste de Impressos', function () {
    this.beforeAll(async () => {
        const connection = MongoDb.connect()
        context = new Context(new MongoDb(connection, ImpressoSchema))
        var gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0
    })
    it('Cadastrar impresso', async function () {
        
        const resultado = await context.create(cadastrar)
        const gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0

        assert.ok(resultado._id === lastId)
    })

    it('Listar impresso: ', async function () {
        const [result] = await context.read({ _id: lastId })

        assert.ok(true)
    })
    it('Remover impresso', async () => {

        const result = await context.delete(lastId)
        assert.deepEqual(result.n, 1)
    })

})
let app ={}
let atID ={}
describe('Teste dos Routes de impresso', function (){
    this.beforeAll(async ()=>{
        app = await Api

    })

    it('Cadastrar impresso - POST', async ()=>{
        cadastrar.nome=cadastrar.nome+' (Via POST)'
        const result = await app.inject({
            method:'POST',
            url:'/impresso',
            payload: cadastrar
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id
        assert.ok(statusCode===200)
        assert.notStrictEqual(_id,undefined)
        assert.deepEqual(message,"Impresso cadastrado com sucesso!")
    })
    it('/impresso -> get - listar',async ()=>{
        const result = await app.inject({
            method:'GET',
            url: '/impresso?skip=0&limit=10'
        })
        const dados = JSON.parse(result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.ok(Array.isArray(dados))
    })
    
    
    it('/impresso -> get - listar por id',async ()=>{
        const id=atID
        const result = await app.inject({
            method:'GET',
            url: `/impresso/${id}`
        })
        const [dados]= JSON.parse( result.payload)
        const statusCode = result.statusCode

        assert.deepEqual(statusCode,200)
        assert.deepEqual(dados._id, id)
    })

    
    it('Atualizar impresso- PATCH', async ()=>{
        const id = atID
        const expected = {
            nome: cadastrar.nome+' (Atualizado)'
        } 
        const result = await app.inject({
            method:'PATCH',
            url:`/impresso/${id}`,
            payload: JSON.stringify(expected)
        })
        
        const statusCode = result.statusCode
        const {message}= JSON.parse( result.payload)
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Impresso atualizado com sucesso!")
    })


    
    it('Remover impresso- DELETE', async ()=>{
        const id = atID
        const result = await app.inject({
            method:'DELETE',
            url:`/impresso/${id}`,
        })
        const statusCode = result.statusCode
        const {message,_id}= JSON.parse( result.payload)
        atID = _id;
        assert.ok(statusCode===200)
        assert.deepEqual(message,"Impresso removido com sucesso!")
    })
})
