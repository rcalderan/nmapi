const assert = require('assert')
const MongoDb = require('../database/strategies/mongodb/mongoDb')
const FuncionarioSchema = require('../database/strategies/mongodb/mongodb/schemas/funcionarioSchemma')
const Context = require('../database/strategies/base/contextStrategy')

// 1o alterar criar pasta mongodb
// 2o mover mongodbStrategy para mongodb
// 3o modificar classe do mongodbStrategy
// 4o modificar criar schema em mongodb/schemas
// 6o modificar teste fazendo conexÃ£o direto do MongoDB
// 5o modificar teste passando para o MongoDB

var MOCK_FUNC_CADASTRAR = {
    _id: 0,
    nome: "DESCONHECIDO",
    sigla: "N/A",
    senha: "nYRAyYaF/Kc=",
    privilegio: 1,
    ativo: true,
};

let context = {}
let lastId = 0
describe('Testando Pessoas', function () {
    this.beforeAll(async () => {
        const connection = MongoDb.connect()
        context = new Context(new MongoDb(connection, FuncionarioSchema))
        var gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0
    })
    it('Cadastrar Pessoa', async function () {
        
        const resultado = await context.create(MOCK_FUNC_CADASTRAR)
        lastId++
        MOCK_FUNC_CADASTRAR._id=lastId
        //assert.equal(resultado,MOCK_FUNC_CADASTRAR)
        assert.ok(resultado._id===lastId)
    })

    it('Listar Pessoa: ', async function () {
        const [result] = await context.read({ _id: lastId })

        assert.ok(result._id==lastId)
    })

    it('Listar Todas Pessoas: ', async function () {
        const result = await context.read()

        assert.ok(Array.isArray(result))
    })

    it('Atualizar Pessoa: ', async () => {
        
        const result = await context.update(lastId, {
            nome: MOCK_FUNC_CADASTRAR.nome+ Math.random().toString()
        })
        assert.deepEqual(result.nModified, 1)
    })
    it('Remover Pessoa: ', async () => {
        const result = await context.delete(lastId)
        assert.deepEqual(result.n, 1)
    })
})
