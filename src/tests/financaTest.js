const assert = require('assert').strict
const MongoDb = require('../database/strategies/mongodb/mongoDb')
const FinancaSchema = require('../database/strategies/mongodb/mongodb/schemas/financaSchema')
const Context = require('../database/strategies/base/contextStrategy')
const { ObjectId } = require('mongoose');

var MOCK_CADASTRAR = {
    _id: 1,
    entrada: 0,
    funcionario: 0,
    hoje: new Date('1981-03-11 00:00:00'),
    data: new Date('1981-03-11 00:00:00'),
    quitado: new Date('1981-03-11 00:00:00'),
    descricao: 'Nada a comunicarx',
    valor: 234,
    done:false,
    categoria: "Diversos",
    destino: "Boleto"
};

let context = {}

let lastId = 0
describe('Testes de finanças', function () {
    this.beforeAll(async () => {
        const connection = MongoDb.connect()
        context = new Context(new MongoDb(connection, FinancaSchema))
        const gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0

    })
    it('Criar finança', async function () {
        const resultado = await context.create(MOCK_CADASTRAR)
        const gotId = await context.lastId()
        lastId = gotId ? gotId._id : 0

        assert.ok(resultado._id === lastId)
    })

    it('Atualizar finança', async () => {
        var rand = Math.random()
        const result = await context.update(lastId, {
            descricao: "Algo assim... " + rand.toString()
        })
        assert.deepEqual(result.nModified, 1)
    })
    it('Listar finança', async function () {
        const [result] = await context.read({ _id: MOCK_CADASTRAR._id }, 0, 10)

        assert.ok(result._id == lastId)
    })

    it('Remover finança', async () => {

        const result = await context.delete(lastId)
        assert.deepEqual(result.n, 1)
    })
})
