const BaseRoute = require('./base/baseRoute')
const Joi = require('joi')
const Boom = require('boom')

const failAction =(request,headers,error) =>{
    throw error
}

class RoupaRoutes extends BaseRoute {
    constructor(db) {
        super()
        this.db = db
    }

    list_roupa() {
        return {
            path: '/api/roupa',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                    query:{
                        skip: Joi.number().integer().default(0),
                        limit: Joi.number().integer().default(10),
                        nome: Joi.string().min(3).max(100)
                    }
                }

            },
            handler: (request, headers) => {
                try{
                    let query ={}
                    const {
                        skip,limit,nome,cpf
                    } = request.query
                    query = nome ? {
                        nome: {$regex: `.*${nome}*.`}
                    } : {}
                    return this.db.read(query,skip,limit)
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    
    listById_roupa() {
        return {
            path: '/api/roupa/{id}',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                    params: {
                        id: Joi.number().integer().required(),
                    }
                }

            },
            handler: async (request) => {
                try{
                    //
                    const {
                        id
                    } = request.params

                    const result = await this.db.read({_id:id},0,1)
                    if (result.length ===0)
                    return Boom.notFound('Roupa não Encontrada')
                    return result
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    create(){
        return {
            path : '/api/roupa',
            method:'POST',
            config:{
                validate:{
                    failAction,
                    payload:{
                        _id: Joi.number().integer(),
                        nome: Joi.string().max(400).required(),
                        obs: Joi.string().min(0).max(500),
                        tamanho: Joi.string().max(4).required(),
                        valor: Joi.number().required(),
                        nloc: Joi.number().integer(),
                        no_estoque: Joi.boolean(),
                        cor: Joi.string().max(10),
                        base: Joi.number(),
                        ajuste: Joi.number(),
                        data: Joi.date().required(),
                        preco_id: Joi.number().integer().required(),
                        status: Joi.number().integer(),
                        tipo: Joi.number().integer().required(),
                    }
                }
            },
            handler: async (request) =>{
                try{
                    const dados = request.payload
                    dados.base = dados.base ? dados.base : dados.valor
                    //console.log('create payload',dados)
                    
                    const result = await this.db.create(dados)
                    //console.log('result',result)
                    if(!result)
                    return Boom.preconditionFailed( "Não foi possível atualizar roupa!")
                    
                    return {
                        message: "Roupa cadastrada com sucesso!",
                        _id: result._id
                    }

                }
                catch(error){
                    return Boom.internal()
                }
            }
        }
    }
    update_roupa() {
        try {
            return {
                path: '/api/roupa/{id}',
                method: 'PATCH',
                config: {
                    validate: {
                        failAction,
                        payload: {
                            nome: Joi.string().max(400),
                            obs: Joi.string().min(0).max(500),
                            tamanho: Joi.string().max(4),
                            valor: Joi.number(),
                            nloc: Joi.number().integer(),
                            no_estoque: Joi.boolean(),
                            cor: Joi.string().max(10),
                            base: Joi.number(),
                            ajuste: Joi.number(),
                            data: Joi.date(),
                            preco_id: Joi.number().integer(),
                            status: Joi.number().integer(),
                            tipo: Joi.number().integer(),
                        },
                        params: {
                            id: Joi.number().integer().required(),
                        }
                    },

                },
                handler: async (request, headers) => {
                    const {
                        id
                    } = request.params
                    const {
                        payload
                    } = request

                    const dadosString = JSON.stringify(payload)
                    const dados = JSON.parse(dadosString) 

                    const result = await this.db.update(id, dados)
                    if (result.nModified !== 1)
                    return Boom.preconditionFailed('Não foi possível atualizar roupa!')
                    return {
                        message: 'Roupa atualizada com sucesso!'
                    }

                }
            }
        } catch (error) {
            return Boom.internal()
        }
    }
    
    delete_roupa() {
        try{
            return {
                path: '/api/roupa/{id}',
                method: 'DELETE',
                config: {
                    validate: {
                        failAction,
                        params: {
                            id: Joi.number().integer().required()
                        }
                    }
                },
                handler: async (request) => {
                    const id = request.params.id;
                    const result = await this.db.delete(id)
                    if(result.n !== 1)
                    return Boom.preconditionFailed('Não foi possível remover roupa')
                    return{
                        message: 'Roupa removido com sucesso!'
                    }
                }
            }
        }catch{
            return Boom.internal()
        }
    }

}

module.exports = RoupaRoutes