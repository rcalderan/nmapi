const BaseRoute = require('./base/baseRoute')
const Joi = require('joi')
const Boom = require('boom')

const failAction =(request,headers,error) =>{
    throw error
}

class AcessorioRoutes extends BaseRoute {
    constructor(db) {
        super()
        this.db = db
    }

    list() {
        return {
            path: '/acessorio',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                    query:{
                        skip: Joi.number().integer().default(0),
                        limit: Joi.number().integer().default(10),
                        sigla: Joi.string()
                    }
                }

            },
            handler: async (request, headers) => {
                try{
                    let query ={}
                    const {
                        skip,limit,sigla
                    } = request.query
                    query = sigla ? {
                        sigla
                    } : {}
                    return this.db.read(query,skip,limit)
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    
    listById() {
        return {
            path: '/acessorio/{sigla}',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                    params: {
                        sigla: Joi.string().required(),
                    }
                }

            },
            handler: async (request) => {
                try{
                    
                    const {
                        sigla
                    } = request.params

                    const result = await this.db.read({sigla:sigla},0,1)
                    if (result.length ===0)
                    return Boom.notFound('Cliente não Encontrado')
                    return result
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    all() {
        return {
            path: '/acessorio/all',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                }

            },
            handler: async (request) => {
                try{
                    const result = await this.db.distinct('sigla')
                    return result
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    create(){
        return {
            path : '/acessorio',
            method:'POST',
            config:{
                validate:{
                    failAction,
                    payload:{
                        _id:Joi.number().integer(),
                        sigla: Joi.string().max(5).required(),
                        nome: Joi.string().max(200).required(),
                    }
                }
            },
            handler: async (request) =>{
                try{
                    const result = await this.db.create(request.payload)
                    
                    if(!result)
                    return Boom.preconditionFailed( "Não foi possível atualizar cliente!")
                    
                    return {
                        message: "Acessorio cadastrado com sucesso!",
                        _id: result._id
                    }

                }
                catch(error){
                    return Boom.internal()
                }
            }
        }
    }
    update() {
        try {
            return {
                path: '/acessorio/{id}',
                method: 'PATCH',
                config: {
                    validate: {
                        failAction,
                        payload: {
                            sigla: Joi.string().max(5),
                            nome: Joi.string().max(200),
                        },
                        params: {
                            id: Joi.number().integer().required(),
                            //nome: Joi.string().max(300),
                        }
                    },

                },
                handler: async (request, headers) => {
                    const {
                        id
                    } = request.params
                    const {
                        payload
                    } = request

                    const dadosString = JSON.stringify(payload)
                    const dados = JSON.parse(dadosString) //<-- aqui ele elimina parametros undefided, a fim de não atualizar dados para undefined

                    const result = await this.db.update(id, dados)
                    if (result.nModified !== 1)
                    return Boom.preconditionFailed('Não foi possível atualizar acessorio!')
                    return {
                        message: 'Acessorio atualizado com sucesso!'
                    }

                }
            }
        } catch (error) {
            return Boom.internal()
        }
    }
    
    delete() {
        try{
            return {
                path: '/acessorio/{id}',
                method: 'DELETE',
                config: {
                    validate: {
                        failAction,
                        params: {
                            id: Joi.number().integer().required()
                        }
                    }
                },
                handler: async (request) => {
                    const id = request.params.id;
                    const result = await this.db.delete(id)
                    if(result.n !== 1)
                    return Boom.preconditionFailed('Não foi possível remover acessorio')
                    return{
                        message: 'Acessorio removido com sucesso!'
                    }
                }
            }
        }catch{
            return Boom.internal()
        }
    }

}

module.exports = AcessorioRoutes