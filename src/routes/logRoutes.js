const BaseRoute = require('./base/baseRoute')
const Joi = require('joi')
const Boom = require('boom')

const failAction =(request,headers,error) =>{
    throw error
}

class LogRoutes extends BaseRoute {
    constructor(db) {
        super()
        this.db = db
    }

    list() {
        return {
            path: '/log',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                    query:{
                        skip: Joi.number().integer().default(0),
                        limit: Joi.number().integer().default(10),
                        who: Joi.number().integer(),
                        from: Joi.date(),
                        to: Joi.date(),
                        type: Joi.number().integer(),
                        machine:Joi.string().max(500),
                        what: Joi.string()
                    }
                }

            },
            handler: async (request, headers) => {
                try{
                    let query ={}
                    const {
                        skip,limit,who,from,to,type,what,machine
                    } = request.query
                    //console.log('query',request.query)
                    //return Boom.notFound()
                    query = what ? {
                        what: {$regex: `.*${what}*.`}
                    } : {}
                    query = who ? {
                        ... query,
                        who
                    } : query
                    query = type ? {
                        ... query,
                        type
                    } : query
                    query = machine ? {
                        ... query,
                        machine: {$regex: `.*${machine}*.`}
                    } : query
                    let between={}
                    between = from ? {
                        $gte: new Date(from)
                    } : {}
                    between = to ? {
                        ... between,
                        $lte: new Date(to)
                    } : between
                    query= Object.keys(between).length ? {
                        ...query,
                        time:between
                    } : query

                    const res = await this.db.read(query,skip,limit)
                    if(res.length===0)
                        return Boom.notFound()
                        else{
                            return res
                        }
                }catch(error){
                    console.log('error',error)
                    return Boom.internal()

                }
            }
        }
    }
    
    create(){
        return {
            path : '/log',
            method:'POST',
            config:{
                validate:{
                    failAction,
                    payload:{
                        _id:Joi.number().integer(),
                        type:Joi.number().integer().required(),
                        time:Joi.date(),
                        machine:Joi.string().max(500).required(),
                        who:Joi.number().integer().required(),
                        what:Joi.string().max(500).required(),
                        
                    }
                }
            },
            handler: async (request) =>{
                try{
                    let query = request.payload
                    const time =new Date(Date.now())
                    query = query.time ? query :{
                        ...query,
                        time
                    }
                    
                    const result = await this.db.create(query)
                    
                    if(!result)
                    return Boom.preconditionFailed( "Não foi possível emitir LOG")
                    
                    return {
                        message: "LOG emitido com sucesso!",
                        _id: result._id
                    }

                }
                catch(error){
                    console.log('error',error)
                    return Boom.internal()
                }
            }
        }
    }
    
    delete() {
        try{
            return {
                path: '/log/{id}',
                method: 'DELETE',
                config: {
                    validate: {
                        failAction,
                        params: {
                            id: Joi.number().integer().required()
                        }
                    }
                },
                handler: async (request) => {
                    const id = request.params.id;
                    const result = await this.db.delete(id)
                    if(result.n !== 1)
                    return Boom.preconditionFailed('Não foi possível remover LOG')
                    return{
                        message: 'LOG removido com sucesso!'
                    }
                }
            }
        }catch{
            return Boom.internal()
        }
    }

}

module.exports = LogRoutes