const BaseRoute = require('./base/baseRoute')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi);
const Boom = require('boom')
const {ObjectId} = require('../database/strategies/mongodb/mongoDb')

const failAction =(request,headers,error) =>{
    throw error
}


let joiItensCreate=Joi.object().keys({
    codigo:Joi.string().required(),
    entregue: Joi.boolean().default(false),
    atendente: Joi.number().integer().required(),
    descricao: Joi.string().min(3).max(150),
    valor:Joi.number().required(),
    sub: Joi.array(),
})
let joiPagamentosCreate=Joi.object().keys({
    data:Joi.date().required(),
    forma: Joi.number().integer().default(0),
    vezes: Joi.number().integer().default(1),
    funcionario: Joi.number().integer().required(),
    valor:Joi.number().required(),
    recibo: Joi.object().keys({
        data: Joi.date().required(),
        extorno: Joi.boolean().default(false),
        hoje: Joi.date(),
        impresso: Joi.boolean().default(false)
    })
})
class ContratoRoutes extends BaseRoute {
    constructor(db) {
        super()
        this.db = db
    }
    async _getLastRecibo(){
        try{
            const query = [
                {$addFields:{
                    "codigo":"$pagamentos.recibo.codigo"
                }},
                {$sort:{"codigo":-1}},
                { $limit:1},
                {$project:{"_id":0,
                    "codigo":{
                        $max:'$codigo'
                    }
                }},
                //{$group:{"_id":null,"last":{$max:"$codigo"}}}
                //{$group:{"$codigo"}}
            ]
            const [result] = await this.db.aggregate(query) 
            const last = parseInt(result.codigo) 
            
            if (last){
                return last
            }
            return 0
        }
        catch(error){
            console.log('error',error);

        }
    }

    list() {
        return {
            path: '/contrato',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                    query:{
                        skip: Joi.number().integer().default(0),
                        limit: Joi.number().integer().default(10),
                    }
                }

            },
            handler:async (request, headers) => {
                try{
                    let query ={}
                    const {
                        skip,limit,nome,id
                    } = request.query
                    
                    query = nome ? {
                        nome: {$regex: `.*${nome}*.`}
                    } : {}
                    query = id ? {
                        ... query,
                        _id: `${id}`
                    } : query
                    return await this.db.read(query,skip,limit)
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    
    listFromDate() {
        return {
            path: '/contrato/date',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                    query:{
                        skip: Joi.number().integer().default(0),
                        limit: Joi.number().integer().default(10),
                        from: Joi.date(),
                        to: Joi.date(),
                    }
                }

            },
            handler:async (request, headers) => {
                try{
                    let query ={}
                    const {
                        skip,limit,from,to
                    } = request.query
                    let match ={baixa:false}
                    let between={}
                    between = from ? {
                        '$gte':from}
                     : between
                     between = to ? {
                        ... between,
                        '$lte':to}
                     : match

                     match= Object.keys(between).length ?{
                         ...match,
                         usa:between
                     }: match
                     
                    query = [
                        {$match:match},
                        //{$group:{_id:'$usa'}},
                        //{$sort:{_id:1}},
                    ]
                    
                    const agreg = await this.db.aggregate(query)
                    if(agreg.length===0)
                        return Boom.notFound()
                        else
                        return agreg
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    
    
    names() {
        return {
            path: '/contrato/names',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                }

            },
            handler:async (request, headers) => {
                try{
                    let query =[
                        { $match: { baixa:false } },
                        { $lookup:{ from: 'cliente', localField: 'cliente', foreignField: '_id', as: 'Cliente' }},
                        {$unwind:'$Cliente'},
                        {$group:{_id:'$Cliente.nome'}},
                        {$unwind:'$_id'},
                        {$sort:{_id:1}},
                        //{$limit:10}
                    ]
                    
                    const agradated = await this.db.aggregate(query)                    
                    let onlynames=[]
                    agradated.forEach(it => {
                        onlynames.push(it._id)
                    });
                    return onlynames
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }

    listById() {
        return {
            path: '/contrato/{id}',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                    params:{
                        id: Joi.number().integer().required()
                    }
                }

            },
            handler:async (request, headers) => {
                try{
                    let query ={}
                    const {
                        id
                    } = request.params                    
                    query = id ? {
                        ... query,
                        _id: `${id}`
                    } : query
                    const result= await this.db.read(query)
                    if(result.length===0){
                        return Boom.notFound()
                    }else{
                        return result
                    }
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    
    getLastRecibo() {
        return {
            path: '/contrato/lastRecibo',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                }

            },
            handler: async (request) => {
                try{
                    return await this._getLastRecibo()
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    create(){
        return {
            path : '/contrato',
            method:'POST',
            config:{
                validate:{
                    failAction,
                    payload:{
                        _id:Joi.number().integer(),
                        cliente: Joi.number().integer().required(),
                        hoje: Joi.date(),
                        retirada: Joi.date().required(),
                        usa: Joi.date().required(),
                        devolucao: Joi.date().required(),
                        devolveu: Joi.date(),
                        tipo:Joi.number().integer(),
                        situacao:Joi.number().integer(),
                        criado_por:Joi.number().integer().required(),
                        baixa_por:Joi.number().integer(),
                        baixa: Joi.boolean(),
                        comunicado: Joi.string().min(0).max(500).required(),
                        itens: Joi.array().items(joiItensCreate),
                        pagamentos: Joi.array().items(joiPagamentosCreate),
                    }
                }
            },
            handler: async (request) =>{
                try{
                    var {
                        pagamentos
                    } = request.payload
                    let codigoToAdd =0
                    for (var i=0; i< pagamentos.length;i++){
                        if(pagamentos[i].forma>0){
                            
                        if(!ContratoRoutes._isEmpity(pagamentos[i].recibo)) {
                            if(!codigoToAdd ){//consultar 1x
                                codigoToAdd =await this._getLastRecibo()
                                codigoToAdd++
                            }
                            pagamentos[i].recibo._id = ObjectId()
                            pagamentos[i].recibo={
                                codigo:codigoToAdd,
                                ...pagamentos[i].recibo
                            }
                        }

                        }else{
                            pagamentos[i].recibo=null
                        }
                    }

                    const result = await this.db.create(request.payload)
                    
                    if(!result)
                    return Boom.preconditionFailed( "Não foi possível atualizar contrato!")
                    
                    return {
                        message: "Contrato cadastrado com sucesso!",
                        _id: result._id
                    }

                }
                catch(error){
                    console.log(error)
                    return Boom.internal()
                }
            }
        }
    }
    update() {
        try {
            return {
                path: '/contrato/{id}',
                method: 'PATCH',
                config: {
                    validate: {
                        failAction,
                        payload: {
                            cliente: Joi.number().integer(),
                            retirada: Joi.date(),
                            usa: Joi.date(),
                            devolucao: Joi.date(),
                            devolveu: Joi.date(),
                            situacao: Joi.number().integer(),
                            tipo: Joi.number().integer(),
                            criado_por: Joi.number().integer(),
                            baixa_por: Joi.number().integer(),
                            comunicado: Joi.string().default(''),
                            baixa: Joi.boolean(),
                            itens: Joi.array(),
                            pagamentos: Joi.array()
                            /*itens: Joi.object().keys({
                                cep: Joi.string(),
                                logradouro: Joi.string(),
                                numero: Joi.number().integer(),
                                bairro: Joi.string(),
                                cidade: Joi.string(),
                                uf: Joi.string().min(2).max(2),
                            }),*/
                        },
                        params: {
                            id: Joi.number().integer().required(),
                            //nome: Joi.string().max(300),
                        }
                    },

                },
                handler: async (request, headers) => {
                    const {
                        id
                    } = request.params
                    const {
                        payload
                    } = request

                    const dadosString = JSON.stringify(payload)
                    const dados = JSON.parse(dadosString) //<-- aqui ele elimina parametros undefided, a fim de não atualizar dados para undefined

                    const result = await this.db.update(id, dados)
                    if (result.nModified !== 1)
                    return Boom.preconditionFailed('Não foi possível atualizar contrato!')
                    return {
                        message: 'Contrato atualizado com sucesso!'
                    }

                }
            }
        } catch (error) {
            return Boom.internal()
        }
    }
    
    delete() {
        try{
            return {
                path: '/contrato/{id}',
                method: 'DELETE',
                config: {
                    validate: {
                        failAction,
                        params: {
                            id: Joi.number().integer().required()
                        }
                    }
                },
                handler: async (request) => {
                    const id = request.params.id;
                    const result = await this.db.delete(id)
                    if(result.n !== 1)
                    return Boom.preconditionFailed('Não foi possível remover contrato')
                    return{
                        message: 'Contrato removido com sucesso!'
                    }
                }
            }
        }catch{
            return Boom.internal()
        }
    }

}

module.exports = ContratoRoutes