const BaseRoute = require('./base/baseRoute')
const Joi = require('joi')
const Boom = require('boom')

const failAction =(request,headers,error) =>{
    throw error
}

class ConfigRoutes extends BaseRoute {
    constructor(db) {
        super()
        this.db = db
    }

    list() {
        return {
            path: '/conf',
            method: 'GET',
            config :{
                validate:{
                    failAction
                }

            },
            handler: () => {
                try{
                    return this.db.read({isDefault:true},0,1)
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    
    create(){
        return {
            path : '/conf',
            method:'POST',
            config:{
                validate:{
                    failAction,
                    payload:{
                        _id:Joi.number().integer(),
                        debugMode: Joi.boolean(),
                        criar_recibos: Joi.boolean(),
                        backupPath: Joi.string().max(500),
                        bkp1: Joi.boolean(),
                        bkp2: Joi.boolean(),
                        bkp3: Joi.boolean(),
                        cnpj: Joi.string().max(500).required(),
                        estadual: Joi.string().max(500).required(),
                        municipal: Joi.string().max(500).required(),
                        empresa: Joi.string().max(500).required(),
                        fantasia: Joi.string().max(500).required(),
                        endereco: Joi.string().max(500).required(),
                        bairro: Joi.string().max(500).required(),
                        numero: Joi.number().max(99999).integer(),
                        uf: Joi.string().max(2).required(),
                        cidade: Joi.string().max(500).required(),
                        cep: Joi.string().min(0).max(10).required(),
                        telefone1: Joi.string().max(15).required(),
                        telefone2: Joi.string().max(15),
                        email: Joi.string().email().required(),
                        site: Joi.string().max(500),
                        isDefault: Joi.boolean(),
                    }
                }
            },
            handler: async (request) =>{
                try{
                    const result = await this.db.create(request.payload)
                    
                    if(!result)
                    return Boom.preconditionFailed( "Não foi possível atualizar as configurações!")
                    
                    return {
                        message: "Configurações cadastradas com sucesso!",
                        _id: result._id
                    }

                }
                catch(error){
                    return Boom.internal()
                }
            }
        }
    }
    update() {
        try {
            return {
                path: '/conf/{id}',
                method: 'PATCH',
                config: {
                    validate: {
                        failAction,
                        payload: {
                            _id:Joi.number().integer(),
                            debugMode: Joi.boolean(),
                            criar_recibos: Joi.boolean(),
                            backupPath: Joi.string().max(500),
                            bkp1: Joi.boolean(),
                            bkp2: Joi.boolean(),
                            bkp3: Joi.boolean(),
                            cnpj: Joi.string().max(500),
                            estadual: Joi.string().max(500),
                            municipal: Joi.string().max(500),
                            empresa: Joi.string().max(500),
                            fantasia: Joi.string().max(500),
                            endereco: Joi.string().max(500),
                            bairro: Joi.string().max(500),
                            numero: Joi.number().max(99999),
                            uf: Joi.string().max(2),
                            cidade: Joi.string().max(500),
                            cep: Joi.string().min(0).max(10),
                            telefone1: Joi.string().max(15),
                            telefone2: Joi.string().max(15),
                            email: Joi.string().email(),
                            site: Joi.string().max(500),
                            isDefault: Joi.boolean(),
                        },
                        params: {
                            id: Joi.number().integer().required(),
                        }
                    },

                },
                handler: async (request, headers) => {
                    const {
                        id
                    } = request.params
                    const {
                        payload
                    } = request

                    const dadosString = JSON.stringify(payload)
                    const dados = JSON.parse(dadosString)

                    const result = await this.db.update(id, dados)
                    if (result.nModified !== 1)
                    return Boom.preconditionFailed('Não foi possível atualizar as configurações!')
                    return {
                        message: 'Configurações atualizadas com sucesso!'
                    }

                }
            }
        } catch (error) {
            return Boom.internal()
        }
    }
    
    delete() {
        try{
            return {
                path: '/conf/{id}',
                method: 'DELETE',
                config: {
                    validate: {
                        failAction,
                        params: {
                            id: Joi.number().integer().required()
                        }
                    }
                },
                handler: async (request) => {
                    const id = request.params.id;
                    const result = await this.db.delete(id)
                    if(result.n !== 1)
                    return Boom.preconditionFailed('Não foi possível remover as Configurações')
                    return{
                        message: 'Configurações removidas com sucesso!'
                    }
                }
            }
        }catch{
            return Boom.internal()
        }
    }

}

module.exports = ConfigRoutes