const BaseRoute = require('./base/baseRoute')
const Joi = require('joi')
const Boom = require('boom')

const failAction =(request,headers,error) =>{
    throw error
}

class PrecoRoutes extends BaseRoute {
    constructor(db) {
        super()
        this.db = db
    }

    list() {
        return {
            path: '/preco',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                    query:{
                        skip: Joi.number().integer().default(0),
                        limit: Joi.number().integer().default(10),
                        nome: Joi.string().min(3).max(100),
                    }
                }

            },
            handler: (request, headers) => {
                try{
                    let query ={}
                    const {
                        skip,limit
                    } = request.query
                    return this.db.read({},skip,limit)
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    
    listByType() {
        return {
            path: '/preco/{tipo}',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                    params: {
                        tipo: Joi.number().integer().required(),
                    }
                }

            },
            handler: async (request) => {
                try{
                    //
                    const {
                        tipo
                    } = request.params

                    const result = await this.db.read({tipo},0,20)
                    if (result.length ===0)
                    return Boom.notFound('Preco não Encontrado')
                    return result
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    listNames() {
        return {
            path: '/preco/names',
            method: 'GET',
            config :{
                validate:{
                    failAction,
                }

            },
            handler: async (request) => {
                try{
                    const result = await this.db.distinct('nome')
                    return result
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    create(){
        return {
            path : '/preco',
            method:'POST',
            config:{
                validate:{
                    failAction,
                    payload:{
                        _id: Joi.number().integer(),
                        nome: Joi.string().max(35).required(),
                        cor: Joi.string().max(35).required(),
                        tipo:Joi.number().integer().required(),
                        valor: Joi.number().integer().required(),
                        enabled: Joi.boolean()
                    }
                }
            },
            handler: async (request) =>{
                try{
                    const result = await this.db.create(request.payload)
                    
                    if(!result)
                    return Boom.preconditionFailed( "Não foi possível atualizar preco!")
                    
                    return {
                        message: "Preco cadastrado com sucesso!",
                        _id: result._id
                    }

                }
                catch(error){
                    return Boom.internal()
                }
            }
        }
    }
    update() {
        try {
            return {
                path: '/preco/{id}',
                method: 'PATCH',
                config: {
                    validate: {
                        failAction,
                        payload: {
                            nome: Joi.string().max(35),
                            cor: Joi.string().max(35),
                            tipo:Joi.number().integer(),
                            valor: Joi.number().integer()
                        },
                        params: {
                            id: Joi.number().integer().required(),
                        }
                    },

                },
                handler: async (request, headers) => {
                    const {
                        id
                    } = request.params
                    const {
                        payload
                    } = request

                    const dadosString = JSON.stringify(payload)
                    const dados = JSON.parse(dadosString) 

                    const result = await this.db.update(id, dados)
                    if (result.nModified !== 1)
                    return Boom.preconditionFailed('Não foi possível atualizar preco!')
                    return {
                        message: 'Preco atualizado com sucesso!'
                    }

                }
            }
        } catch (error) {
            return Boom.internal()
        }
    }
    
    delete() {
        try{
            return {
                path: '/preco/{id}',
                method: 'DELETE',
                config: {
                    validate: {
                        failAction,
                        params: {
                            id: Joi.number().integer().required()
                        }
                    }
                },
                handler: async (request) => {
                    const id = request.params.id;
                    const result = await this.db.delete(id)
                    if(result.n !== 1)
                    return Boom.preconditionFailed('Não foi possível remover preco')
                    return{
                        message: 'Preco removido com sucesso!'
                    }
                }
            }
        }catch{
            return Boom.internal()
        }
    }

}

module.exports = PrecoRoutes