class BaseRoute {
    static methods() {
        return Object.getOwnPropertyNames(this.prototype)
            .filter(method => method !== 'constructor' && !method.startsWith('_'))
    }
    static _isEmpity(object){
        try{
            if (Object.keys(object).length)
            return false
            else
            return true
        }
        catch{
            return true
        }
    }
}

module.exports = BaseRoute