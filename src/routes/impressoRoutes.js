const BaseRoute = require('./base/baseRoute')
const Joi = require('joi')
const Boom = require('boom')

const failAction = (request, headers, error) => {
    throw error
}

class ImpressoRoutes extends BaseRoute {
    constructor(db) {
        super()
        this.db = db
    }

    list() {
        return {
            path: '/impresso',
            method: 'GET',
            config: {
                validate: {
                    failAction,
                    query: {
                        skip: Joi.number().integer().default(0),
                        limit: Joi.number().integer().default(10),
                    }
                }
            },
            handler: (request, headers) => {
                try {
                    let query = {}
                    const {
                        skip, limit
                    } = request.query
                    return this.db.read(query, skip, limit)
                } catch (error) {
                    return Boom.internal()

                }
            }
        }
    }

    listById() {
        return {
            path: '/impresso/{id}',
            method: 'GET',
            config: {
                validate: {
                    failAction,
                    params: {
                        id: Joi.number().integer().required(),
                    }
                }

            },
            handler: async (request) => {
                try {
                    //
                    const {
                        id
                    } = request.params

                    const result = await this.db.read({ _id: id }, 0, 1)
                    if (result.length === 0)
                        return Boom.notFound('Impresso não Encontrado')
                    return result
                } catch (error) {
                    return Boom.internal()

                }
            }
        }
    }
    create() {
        return {
            path: '/impresso',
            method: 'POST',
            config: {
                validate: {
                    failAction,
                    payload: {
                        _id: Joi.number().integer(),
                        nome: Joi.string().min(3).max(400).required(),
                        data: Joi.date().required(),
                        isDefault: Joi.boolean().required(),
                        content: Joi.array(),
                        type: Joi.string().min(2).max(50).required(),
                    }
                }
            },
            handler: async (request) => {
                try {
                    const result = await this.db.create(request.payload)

                    if (!result)
                        return Boom.preconditionFailed("Não foi possível atualizar impresso!")

                    return {
                        message: "Impresso cadastrado com sucesso!",
                        _id: result._id
                    }

                }
                catch (error) {
                    return Boom.internal()
                }
            }
        }
    }
    update() {
        try {
            return {
                path: '/impresso/{id}',
                method: 'PATCH',
                config: {
                    validate: {
                        failAction,
                        payload: {
                            nome: Joi.string().min(3).max(400),
                            data: Joi.date(),
                            isDefault: Joi.boolean(),
                            content: Joi.array(),
                            type: Joi.string().min(2).max(50),
                        },
                        params: {
                            id: Joi.number().integer().required(),
                        }
                    },

                },
                handler: async (request, headers) => {
                    const {
                        id
                    } = request.params
                    const {
                        payload
                    } = request

                    const dadosString = JSON.stringify(payload)
                    const dados = JSON.parse(dadosString) //<-- aqui ele elimina parametros undefided, a fim de não atualizar dados para undefined

                    const result = await this.db.update(id, dados)
                    if (result.nModified !== 1)
                        return Boom.preconditionFailed('Não foi possível atualizar impresso!')
                    return {
                        message: 'Impresso atualizado com sucesso!'
                    }

                }
            }
        } catch (error) {
            return Boom.internal()
        }
    }

    delete() {
        try {
            return {
                path: '/impresso/{id}',
                method: 'DELETE',
                config: {
                    validate: {
                        failAction,
                        params: {
                            id: Joi.number().integer().required()
                        }
                    }
                },
                handler: async (request) => {
                    const id = request.params.id;
                    const result = await this.db.delete(id)
                    if (result.n !== 1)
                        return Boom.preconditionFailed('Não foi possível remover impresso')
                    return {
                        message: 'Impresso removido com sucesso!'
                    }
                }
            }
        } catch{
            return Boom.internal()
        }
    }

}

module.exports = ImpressoRoutes