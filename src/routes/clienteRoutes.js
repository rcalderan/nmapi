const BaseRoute = require('./base/baseRoute')
const Joi = require('joi')
const Boom = require('boom')

const failAction =(request,headers,error) =>{
    throw error
}

class ClienteRoutes extends BaseRoute {
    constructor(db) {
        super()
        this.db = db
        this.baseRoute='/api'
    }

    list() {
        return {
            path: `${this.baseRoute}/cliente`,
            method: 'GET',
            config :{
                validate:{
                    failAction,
                    query:{
                        skip: Joi.number().integer().default(0),
                        limit: Joi.number().integer().default(10),
                        nome: Joi.string().min(3).max(100),
                        cpf: Joi.string().length(14) 
                    }
                }

            },
            handler: (request, headers) => {
                try{
                    let query ={}
                    const {
                        skip,limit,nome,cpf
                    } = request.query
                    query = nome ? {
                        nome: {$regex: `.*${nome}*.`}
                    } : {}
                    query = cpf ? {
                        ... query,
                        cpf: `${cpf}`
                    } : query
                    return this.db.read(query,skip,limit)
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    
    listById() {
        return {
            path: `${this.baseRoute}/cliente/{id}`,
            method: 'GET',
            config :{
                validate:{
                    failAction,
                    params: {
                        id: Joi.number().integer().required(),
                    }
                }

            },
            handler: async (request) => {
                try{
                    //
                    const {
                        id
                    } = request.params

                    const result = await this.db.read({_id:id},0,1)
                    if (result.length ===0)
                    return Boom.notFound('Cliente não Encontrado')
                    return result
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    listNames() {
        return {
            path: `${this.baseRoute}/cliente/names`,
            method: 'GET',
            config :{
                validate:{
                    failAction,
                }

            },
            handler: async (request) => {
                try{
                    const result = await this.db.distinct('nome')
                    return result
                }catch(error){
                    return Boom.internal()

                }
            }
        }
    }
    create(){
        return {
            path : `${this.baseRoute}/cliente`,
            method:'POST',
            config:{
                validate:{
                    failAction,
                    payload:{
                        _id:Joi.number().integer(),
                        nome: Joi.string().max(400).required(),
                        cpf: Joi.string().min(14).max(14).required(),
                        rg: Joi.string().max(14),
                        nascimento: Joi.date().required(),
                        sexo: Joi.boolean().required(),
                        autenticacao: Joi.boolean().required(),
                        fones: Joi.array().required(),
                        obs: Joi.string().min(0).max(500).required(),
                        email: Joi.string(),
                        por: Joi.number().required(),
                        endereco: Joi.object().keys({
                            cep:Joi.string(),
                            logradouro: Joi.string(),
                            numero: Joi.number().integer(),
                            bairro: Joi.string(),
                            cidade:Joi.string(),
                            uf: Joi.string().min(2).max(2),
                        }).required(),
                    }
                }
            },
            handler: async (request) =>{
                try{
                    const result = await this.db.create(request.payload)
                    
                    if(!result)
                    return Boom.preconditionFailed( "Não foi possível atualizar cliente!")
                    
                    return {
                        message: "Cliente cadastrado com sucesso!",
                        _id: result._id
                    }

                }
                catch(error){
                    return Boom.internal()
                }
            }
        }
    }
    update() {
        try {
            return {
                path: `${this.baseRoute}/cliente/{id}`,
                method: 'PATCH',
                config: {
                    validate: {
                        failAction,
                        payload: {
                            nome: Joi.string().max(400),
                            cpf: Joi.string().min(14).max(14),
                            rg: Joi.string().max(14),
                            nascimento: Joi.date(),
                            sexo: Joi.boolean(),
                            autenticacao: Joi.boolean(),
                            fones: Joi.array(),
                            obs: Joi.string().min(0).max(500),
                            email: Joi.string(),
                            por: Joi.number(),
                            endereco: Joi.object().keys({
                                cep: Joi.string(),
                                logradouro: Joi.string(),
                                numero: Joi.number().integer(),
                                bairro: Joi.string(),
                                cidade: Joi.string(),
                                uf: Joi.string().min(2).max(2),
                            }),
                        },
                        params: {
                            id: Joi.number().integer().required(),
                            //nome: Joi.string().max(300),
                        }
                    },

                },
                handler: async (request, headers) => {
                    const {
                        id
                    } = request.params
                    const {
                        payload
                    } = request

                    const dadosString = JSON.stringify(payload)
                    const dados = JSON.parse(dadosString) //<-- aqui ele elimina parametros undefided, a fim de não atualizar dados para undefined

                    const result = await this.db.update(id, dados)
                    if (result.nModified !== 1)
                    return Boom.preconditionFailed('Não foi possível atualizar cliente!')
                    return {
                        message: 'Cliente atualizado com sucesso!'
                    }

                }
            }
        } catch (error) {
            return Boom.internal()
        }
    }
    
    delete() {
        try{
            return {
                path: `${this.baseRoute}/cliente/{id}`,
                method: 'DELETE',
                config: {
                    validate: {
                        failAction,
                        params: {
                            id: Joi.number().integer().required()
                        }
                    }
                },
                handler: async (request) => {
                    const id = request.params.id;
                    const result = await this.db.delete(id)
                    if(result.n !== 1)
                    return Boom.preconditionFailed('Não foi possível remover cliente')
                    return{
                        message: 'Cliente removido com sucesso!'
                    }
                }
            }
        }catch{
            return Boom.internal()
        }
    }

}

module.exports = ClienteRoutes