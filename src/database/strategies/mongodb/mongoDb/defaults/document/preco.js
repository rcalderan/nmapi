
const precoDefaultDocument = [
    {_id:0,nome:"N/A",cor:"white",tipo:0,valor:0,enabled:true},
    {_id:1,nome:"Gold",cor:"gold",tipo:3,valor:690,enabled:true},
    {_id:2,nome:"Silver",cor:"silver",tipo:3,valor:490,enabled:true},
    {_id:3,nome:"Red",cor:"red",tipo:3,valor:390,enabled:true},
    {_id:4,nome:"Blue",cor:"blue",tipo:3,valor:290,enabled:true},
    {_id:5,nome:"Green",cor:"green",tipo:3,valor:190,enabled:true},
    {_id:6,tipo:3,valor:140,enabled:true,nome:"Black",cor:"black"},

    {_id:7,tipo:4,valor:290,enabled:true,nome:"Gold",cor:"gold"},
    {_id:8,tipo:4,valor:220,enabled:true,nome:"Silver",cor:"silver",},
    {_id:9,tipo:4,valor:190,enabled:true,nome:"Red",cor:"red",},
    {_id:10,tipo:4,valor:140,enabled:true,nome:"Blue",cor:"blue",},
    {_id:11,tipo:4,valor:100,enabled:true,nome:"Green",cor:"green",},
    {_id:12,tipo:4,valor:80,enabled:true,nome:"Black",cor:"black",},
    
    {_id:13,tipo:5,valor:1300,enabled:true,nome:"Gold",cor:"gold"},
    {_id:14,tipo:5,valor:1000,enabled:true,nome:"Silver",cor:"silver",},
    {_id:15,tipo:5,valor:780,enabled:true,nome:"Red",cor:"red",},
    {_id:16,tipo:5,valor:500,enabled:true,nome:"Blue",cor:"blue",},
    {_id:17,tipo:5,valor:450,enabled:true,nome:"Green",cor:"green",},
    {_id:18,tipo:5,valor:350,enabled:true,nome:"Promo",cor:"black",},
    
    {_id:19,tipo:6,valor:450,enabled:true,nome:"Gold",cor:"gold"},
    {_id:20,tipo:6,valor:350,enabled:true,nome:"Silver",cor:"silver",},
    {_id:21,tipo:6,valor:250,enabled:true,nome:"Red",cor:"red",},
    {_id:22,tipo:6,valor:190,enabled:true,nome:"Blue",cor:"blue",},
    {_id:23,tipo:6,valor:150,enabled:true,nome:"Green",cor:"green",},
    {_id:24,tipo:6,valor:130,enabled:true,nome:"Promo",cor:"black",},
    
    {_id:25,tipo:2,valor:790,enabled:true,nome:"Gold",cor:"gold"},
    {_id:26,tipo:2,valor:590,enabled:true,nome:"Silver",cor:"silver",},
    {_id:27,tipo:2,valor:490,enabled:true,nome:"Red",cor:"red",},
    {_id:28,tipo:2,valor:390,enabled:true,nome:"Blue",cor:"blue",},
    {_id:29,tipo:2,valor:290,enabled:true,nome:"Green",cor:"green",},
    {_id:30,tipo:2,valor:220,enabled:true,nome:"Promo",cor:"black",}, 

    
    {_id:31,tipo:1,valor:6900,enabled:true,nome:"Gold",cor:"gold"},
    {_id:32,tipo:1,valor:5900,enabled:true,nome:"Silver",cor:"silver",},
    {_id:33,tipo:1,valor:4900,enabled:true,nome:"Red",cor:"red",},
    {_id:34,tipo:1,valor:3900,enabled:true,nome:"Blue",cor:"blue",},
    {_id:35,tipo:1,valor:2900,enabled:true,nome:"Green",cor:"green",},
    {_id:36,tipo:1,valor:1900,enabled:true,nome:"Promo",cor:"black",},
    
    {_id:37,tipo:3,valor:590,enabled:true,nome:"Yellow",cor:"gold"},
    {_id:38,tipo:3,valor:690,enabled:true,nome:"Orange",cor:"silver",},
    {_id:39,tipo:3,valor:890,enabled:true,nome:"Violet",cor:"red",},
    {_id:40,tipo:4,valor:390,enabled:true,nome:"Blue",cor:"blue",},
    {_id:41,tipo:4,valor:490,enabled:true,nome:"Green",cor:"green",},
    {_id:42,tipo:4,valor:590,enabled:true,nome:"Promo",cor:"black",},
    
    {_id:43,tipo:5,valor:1300,enabled:true,nome:"Gold",cor:"gold"},
    {_id:44,tipo:5,valor:1500,enabled:true,nome:"Silver",cor:"silver",},
    {_id:45,tipo:5,valor:1900,enabled:true,nome:"Red",cor:"red",},
    {_id:46,tipo:6,valor:550,enabled:true,nome:"Blue",cor:"blue",},
    {_id:47,tipo:6,valor:650,enabled:true,nome:"Green",cor:"green",},
    {_id:48,tipo:6,valor:750,enabled:true,nome:"Promo",cor:"black",},
    
    {_id:49,tipo:1,valor:6900,enabled:true,nome:"Gold",cor:"gold"},
    {_id:50,tipo:1,valor:4900,enabled:true,nome:"Silver",cor:"silver",},
    {_id:51,tipo:1,valor:8900,enabled:true,nome:"Red",cor:"red",},
    {_id:52,tipo:2,valor:590,enabled:true,nome:"Blue",cor:"blue",},
    {_id:53,tipo:2,valor:690,enabled:true,nome:"Green",cor:"green",},
    {_id:54,tipo:2,valor:790,enabled:true,nome:"Promo",cor:"black",},
    /*
    {_id:55,tipo:0,valor:0,enabled:true,nome:"Gold",cor:"gold"},
    {_id:56,tipo:0,valor:0,enabled:true,nome:"Silver",cor:"silver",},
    {_id:57,tipo:0,valor:0,enabled:true,nome:"Red",cor:"red",},
    {_id:58,tipo:0,valor:0,enabled:true,nome:"Blue",cor:"blue",},
    {_id:59,tipo:0,valor:0,enabled:true,nome:"Green",cor:"green",},
    {_id:60,tipo:0,valor:0,enabled:true,nome:"Promo",cor:"black",},
    */
]

module.exports = precoDefaultDocument