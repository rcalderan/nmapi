

const confDefaultDocument = [{
    _id: 0,
    debugMode: true,
    criar_recibos: true,
    backupPath: 'f:\"',
    bkp1: false,
    bkp2: false,
    cnpj: "08.299.621/0001-20",
    estadual: "637.287.665.118",
    municipal: "51.197",
    empresa: "C & K LOCACAO DE ROUPAS LTDA-ME",
    fantasia: "Noiva Modas",
    endereco: "Rua Jesuíno de Arruda",
    numero: 1837, bairro: "Centro", cidade: "São Carlos",
    uf: "SP", cep: "13560-642", telefone1: "(16) 33722363",
    telefone2: "(16) 99702-7631",
    email: "noivamodas@live.com",
    site: "www.noivamodas.com.br",
    isDefault: true
}]

module.exports = confDefaultDocument