const bandeiraDefaultDocument = [{
    _id: 0,
    nome: "Pendente",
    sigla: "NA",
    isActive: true,
}, {
    _id: 1,
    nome: "Dinheiro",
    sigla: "DIN",
    isActive: true,
}, {
    _id: 2,
    nome: "Cheque",
    sigla: "CHK",
    isActive: true,
}, {
    _id: 3,
    nome: "Debito",
    sigla: "DEB",
    isActive: true,
}, {
    _id: 4,
    nome: "Credito",
    sigla: "CRD",
    isActive: true,
}, {
    _id: 5,
    nome: "Tranferência (TED)",
    sigla: "TED",
    isActive: false,
}, {
    _id: 6,
    nome: "Tranferência (DOC)",
    sigla: "DOC",
    isActive: false,
}, {
    _id: 7,
    nome: "Boleto",
    sigla: "BOL",
    isActive: false,
}];
module.exports = bandeiraDefaultDocument