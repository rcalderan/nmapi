
const roupaTipoDefaultDocument = [
    { _id: 0, codigo: "N/A", nome: "Desconhecodo", descricao: "Tipo desconhecido" },
    { _id: 1, codigo: "N", nome: "Noiva", descricao: "Vestido de Noiva" },
    { _id: 2, codigo: "C", nome: "Rigor", descricao: "Traje a Rigor" },
    { _id: 3, codigo: "V", nome: "Vestido", descricao: "Vestido Feminino" },
    { _id: 4, codigo: "T", nome: "Terno", descricao: "Terno Masculino" },
    { _id: 5, codigo: "Q", nome: "Debutante", descricao: "Vestido de Debutante" },
    { _id: 6, codigo: "D", nome: "Daminha", descricao: "Vestido Infantil Feminino" }
]

module.exports = roupaTipoDefaultDocument