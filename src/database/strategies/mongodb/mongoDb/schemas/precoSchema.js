const _COLLECTION_NAME='preco'
const mongoose=  require('mongoose')
const _DEFAULTS = require('../defaults/document/preco')
const schemaOptions ={ 
    collection: _COLLECTION_NAME,
    versionKey: false
}

    
const precoSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    tipo: {
        type: Number,
        required: true
    },
    valor: {
        type: Number,
        required: true
    },
    enabled: {
        type: Boolean,
        default: true
    },
    nome: {
        type: String,
        required: true
    },
    cor: {
        type: String,
        required: true
    }
},schemaOptions);

precoSchema.methods.collumns = function () {
    return {_id:1, nome: 1,tipo:1,valor:1,cor:1,enabled:1}
}

precoSchema.statics.GetDefaultValues = function(){
    return _DEFAULTS
}

precoSchema.statics.GetCollecionName = function(){
    return _COLLECTION_NAME
}
module.exports = mongoose.model(_COLLECTION_NAME, precoSchema) 


