const _COLLECTION_NAME = 'bandeira'

const _DEFAULTS = require('../defaults/document/bandeira')
 
const mongoose = require('mongoose')
const schemaOptions = {
    collection: _COLLECTION_NAME,
    versionKey: false
}
const bandeiraSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    sigla: {
        type: String,
        required: true
    },

    isActive: {
        type: Boolean,
        required: true
    },
}, schemaOptions);

bandeiraSchema.methods.collumns = function () {
    return { _id: 1, nome: 1, sigla: 1, isActive: 1 }
}

bandeiraSchema.statics.GetDefaultValues = function(){
    return _DEFAULTS
}

bandeiraSchema.statics.GetCollecionName = function(){
    return _COLLECTION_NAME
}


module.exports = mongoose.model(_COLLECTION_NAME, bandeiraSchema)


