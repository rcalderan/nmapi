const _COLLECTION_NAME='cliente'
const mongoose=  require('mongoose')
const schemaOptions ={ 
    collection: _COLLECTION_NAME,
    versionKey: false
}

const _DEFAULTS=[]

const clienteSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    cpf: {
        type: String,
        required: true
    },
    rg: {
        type: String,
        required: true
    },
    nascimento: {
        type: Date,
        required: true
    },
    sexo:  {
        type: Boolean,
        required: true
    },
    autenticacao: {
        type: Boolean,
        required: true
    },
    fones: Array,
    obs:  {
        type: String,
        required: true
    },
    email:  {
        type: String,
        required: true
    },
    por: {
        type: Number,
        required: true
    },
    endereco:  {
        type: Object,
        required: true
    },
},schemaOptions);

clienteSchema.methods.collumns = function () {
    return {_id:1, nome: 1, cpf: 1,autenticacao:1,sexo:1,email:1, rg:1,nascimento:1,obs:1,por:1,fones:1,endereco: 1}
}

clienteSchema.statics.GetDefaultValues = function(){
    return _DEFAULTS
}

clienteSchema.statics.GetCollecionName = function(){
    return _COLLECTION_NAME
}

module.exports = mongoose.model(_COLLECTION_NAME, clienteSchema) 


