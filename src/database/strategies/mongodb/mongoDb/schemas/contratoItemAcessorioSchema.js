const _COLLECTION_NAME='contrato_item_acessorio'
const mongoose=  require('mongoose')
const _DEFAULTS = require('../defaults/document/contratoAcessorio')
const schemaOptions ={ 
    collection: _COLLECTION_NAME,
    versionKey: false
}

const contratoItemAcessorioSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    sigla: {
        type: String,
        required: true
    },
},schemaOptions);

const _collums = {
    _id: 1, 
    sigla: 1,
     nome: 1,
}
contratoItemAcessorioSchema.methods.collumns = function () {
    return _collums
}

contratoItemAcessorioSchema.statics.GetDefaultValues = function(){
    return _DEFAULTS
}

contratoItemAcessorioSchema.statics.GetCollecionName = function(){
    return _COLLECTION_NAME
}
module.exports = mongoose.model(_COLLECTION_NAME, contratoItemAcessorioSchema) 


