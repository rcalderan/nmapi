const _COLLECTION_NAME='roupa_tipo'
const mongoose=  require('mongoose')
const _DEFAULTS = require('../defaults/document/roupaTipo')
const schemaOptions ={ 
    collection: _COLLECTION_NAME,
    versionKey: false
}

const roupaTipoSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    codigo: {
        type: String,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    descricao: {
        type: String,
        required: true
    }
},schemaOptions);

roupaTipoSchema.methods.collumns = function () {
    return {_id:1, nome: 1,codigo:1,descricao:1}
}

roupaTipoSchema.statics.GetDefaultValues = function(){
    return _DEFAULTS
}

roupaTipoSchema.statics.GetCollecionName = function(){
    return _COLLECTION_NAME
}
module.exports = mongoose.model(_COLLECTION_NAME, roupaTipoSchema) 


