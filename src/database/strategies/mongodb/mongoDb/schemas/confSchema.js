const _COLLECTION_NAME ='conf'
const mongoose=  require('mongoose')
const _DEFAULTS = require('../defaults/document/conf')
const schemaOptions ={ 
    collection: _COLLECTION_NAME,
    versionKey: false
}
const _collums = {
    _id: 1,
    debugMode: 1,
    criar_recibos: 1,
    backupPath: 1,
    bkp1: 1,
    bkp2: 1,
    cnpj: 1,
    estadual: 1,
    municipal: 1,
    empresa: 1,
    fantasia: 1,
    endereco: 1,
    numero: 1837,
    bairro: 1,
    cidade: 1,
    uf: 1, cep: 1
    , telefone1: 1,
    telefone2: 1,
    email: 1,
    site: 1,
    isDefault: 1,
}
const confSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    debugMode: {
        type: Boolean,
        default: false
    },
    criar_recibos: {
        type: Boolean,
        default: false
    },
    backupPath: {
        type: String,
        default: ''
    },
    bkp1: {
        type: Boolean,
        default: false
    },
    bkp2:  {
        type: Boolean,
        default: false
    },
    cnpj: {
        type: String,
        required: true
    },
    estadual: {
        type: String,
        required: true
    },
    municipal:  {
        type: String,
        required: true
    },
    empresa:  {
        type: String,
        required: true
    },
    fantasia:  {
        type: String,
        required: true
    },
    endereco:  {
        type: String,
        required: true
    },
    numero: {
        type: Number,
        required: true
    },
    bairro:  {
        type: String,
        required: true
    },
    cidade:  {
        type: String,
        required: true
    },
    uf:  {
        type: String,
        required: true
    },
    cep:  {
        type: String,
        required: true
    },
    telefone1:  {
        type: String,
        required: true
    },
    telefone2:  {
        type: String,
        default: ''
    },
    email:  {
        type: String,
        required: true
    },
    site:  {
        type: String,
        default: ''
    },
    isDefault:  {
        type: Boolean,
        default: false
    },
},schemaOptions);

confSchema.methods.collumns = function () {
    return _collums
}

confSchema.statics.GetDefaultValues = function(){
    return _DEFAULTS
}

confSchema.statics.GetCollecionName = function(){
    return _COLLECTION_NAME
}
module.exports = mongoose.model(_COLLECTION_NAME, confSchema) 

