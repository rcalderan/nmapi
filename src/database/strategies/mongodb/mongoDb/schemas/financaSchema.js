const _COLLECTION_NAME='financa'
const mongoose=  require('mongoose')
const schemaOptions ={ 
    collection: _COLLECTION_NAME,
    versionKey: false
}
const _DEFAULTS = []
const financaSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    entrada: {
        type: Boolean,
        required: true
    },
    funcionario: {
        type: Number,
        required: true
    },
    hoje: {
        type: Date,
        required: true
    },
    data: {
        type: Date,
        required: true
    },
    quitado: {
        type: Date,
        required: true
    },
    descricao:  {
        type: String,
        required: true
    },
    valor: {
        type: Number,
        required: true
    },
    done: {
        type: Boolean,
        required: true
    },
    categoria:  {
        type: String,
        required: true
    },
    destino:  {
        type: String,
        required: true
    },
},schemaOptions);

financaSchema.methods.collumns = function () {
    return {_id:1,entrada:1,funcionario:1, hoje: 1, data: 1,quitado:1,descricao:1,valor:1, done:1,categoria:1,destino:1}
}

financaSchema.statics.GetDefaultValues = function(){
    return _DEFAULTS
}

financaSchema.statics.GetCollecionName = function(){
    return _COLLECTION_NAME
}
module.exports = mongoose.model(_COLLECTION_NAME, financaSchema) 


