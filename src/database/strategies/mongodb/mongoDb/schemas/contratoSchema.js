const _COLLECTION_NAME='contrato'
const mongoose=  require('mongoose')
const schemaOptions ={ 
    collection: _COLLECTION_NAME,
    versionKey: false
}
var ObjectId = mongoose.Schema.Types.ObjectId;
const _DEFAULTS = []
const contratoSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    cliente: {
        type: Number,
        required: true
    },
    retirada: {
        type: Date,
        required: true
    },
    usa: {
        type: Date,
        required: true
    },
    devolucao: {
        type: Date,
        required: true
    },
    devolveu: {
        type: Date,
    },
    situacao: {
        type: Number,
        default:0
    },
    tipo: {
        type: Number,
        default:1
    },
    criado_por:  {
        type: Number,
        required: true
    },
    baixa_por:{
        type: Number,
        default:0
    },
    baixa: {
        type: Boolean,
        default : false
    },
    comunicado:  {
        type: String,
        default:''
    },
    itens:  {
        type: Array,
        required: true
    },
    pagamentos:  [{
        _id: {
            type: ObjectId
        },
        data:{
            type: Date,
            required: true
        },
        forma:{
            type: Number,
            default:0
        },
        valor:{
            type: Number,
            required:true
        },
        vezes:{
            type: Number,
            default:1
        },
        funcionario:{
            type: Number,
            required:true
        },
        recibo:{
            
            type: {
                _id: ObjectId,
                codigo: {
                    type: Number,
                    required: true
                },
                data:{
                    type: Date,
                    required: true
                },
                hoje:{
                    type: Date
                },
                extorno:{
                    type: Boolean,
                    default:false
                },
                impresso:{
                    type: Boolean,
                    default:false
                },
            
            },
            default: null,
            
        }
    }],
},schemaOptions);

contratoSchema.methods.collumns = function () {
    return {_id:1,cliente:1,usa:1, retirada: 1, devolucao: 1,devolveu:1,situacao:1,tipo:1, criado_por:1,baixa_por:1,baixa:1,comunicado:1,itens:1,pagamentos: 1}
}

contratoSchema.statics.GetDefaultValues = function(){
    return _DEFAULTS
}

contratoSchema.statics.GetCollecionName = function(){
    return _COLLECTION_NAME
}
module.exports = mongoose.model(_COLLECTION_NAME, contratoSchema) 


