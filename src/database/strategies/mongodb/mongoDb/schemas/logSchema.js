const _COLLECTION_NAME='log'
const mongoose=  require('mongoose')
const schemaOptions ={ 
    collection: _COLLECTION_NAME,
    versionKey: false
}

const _DEFAULTS = []
const logSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    type: {
        type: Number,
        required: true
    },
    time: {
        type: Date
    },
    who: {
        type: Number,
        required: true
    },
    machine: {
        type: String,
        required: true
    },
    what:  {
        type: String,
        required: true
    },
},schemaOptions);

logSchema.methods.collumns = function () {
    return {_id:1, type: 1, time: 1,who:1,machine: 1,what:1}
}

logSchema.statics.GetDefaultValues = function(){
    return _DEFAULTS
}

logSchema.statics.GetCollecionName = function(){
    return _COLLECTION_NAME
}
module.exports = mongoose.model(_COLLECTION_NAME, logSchema) 


