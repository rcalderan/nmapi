const _COLLECTION_NAME = 'impresso'
const mongoose=  require('mongoose')
const _DEFAULTS = require('../defaults/document/impresso')
const schemaOptions ={ 
    collection: _COLLECTION_NAME,
    versionKey: false
}

const _collums = {
    _id: 1,
    nome: 1,
    type: 1,
    content: 1,
    isDefault: 1,
    data: 1,
}
const impressoSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    content: {
        type: Array,
        default:[]
    },
    isDefault: {
        type: Boolean,
        default: false
    },
    data:  {
        type: Date,
        required: true
    },
},schemaOptions);

impressoSchema.methods.collumns = function () {
    return _collums
}

impressoSchema.statics.GetDefaultValues = function(){
    return _DEFAULTS
}

impressoSchema.statics.GetCollecionName = function(){
    return _COLLECTION_NAME
}
module.exports = mongoose.model( _COLLECTION_NAME, impressoSchema) 