const _COLLECTION_NAME='funcionario'
const mongoose=  require('mongoose')
const _DEFAULTS = require('../defaults/document/funcionario')
const schemaOptions ={ 
    collection: _COLLECTION_NAME,
    versionKey: false
}
const funcionarioSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    sigla: {
        type: String,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    senha: {
        type: String,
        required: true
    },
    privilegio:  {
        type: Number,
        required: true
    },
    ativo: {
        type: Boolean,
        required: true
    },
},schemaOptions);

funcionarioSchema.methods.collumns = function () {
    return {_id:1, sigla: 1, nome: 1,senha:1,privilegio:1,ativo:1}
}

funcionarioSchema.statics.GetDefaultValues = function(){
    return _DEFAULTS
}

funcionarioSchema.statics.GetCollecionName = function(){
    return _COLLECTION_NAME
}
module.exports = mongoose.model(_COLLECTION_NAME, funcionarioSchema) 


