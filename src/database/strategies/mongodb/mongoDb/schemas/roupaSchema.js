const _COLLECTION_NAME='roupa'
const mongoose=  require('mongoose')
const schemaOptions ={ 
    collection: _COLLECTION_NAME,
    versionKey: false
}

const _DEFAULTS = []
const roupaSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    obs: {
        type: String,
        default: ''
    },
    valor: {
        type: Number,
        required: true
    },
    tamanho: {
        type: String,
        required: true
    },
    nloc:  {
        type: Number,
        default: 0
    },
    no_estoque: {
        type: Boolean,
        default: true
    },
    cor: {
        type: String,
        default: 'white'
    },
    base:  {
        type: Number,
        default: 0
    },
    ajuste:  {
        type: Number,
        default: 0
    },
    data: {
        type: Date,
        required: true
    },
    preco_id:  {
        type: Number,
        required: true
    },
    status:  {
        type: Number,
        default: 0
    },
    tipo:  {
        type: Number,
        required: true
    },
},schemaOptions);

roupaSchema.methods.collumns = function () {
    return {_id:1, nome: 1, obs: 1,valor:1,tamanho:1,nloc:1, no_estoque:1,cor:1,base:1,ajuste:1,data:1,preco_id: 1,status:1,tipo:1}
}

roupaSchema.statics.GetDefaultValues = function(){
    return _DEFAULTS
}

roupaSchema.statics.GetCollecionName = function(){
    return _COLLECTION_NAME
}
module.exports = mongoose.model(_COLLECTION_NAME, roupaSchema) 


